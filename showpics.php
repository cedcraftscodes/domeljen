<?php 
include 'admin/config/config.php'; 
$stmt = $conn->prepare("SELECT * from tblevents WHERE `eventid`=:id LIMIT 1");
$stmt->bindParam(':id',$_GET['eventid']);
$stmt->execute(); 
$row = $stmt->fetch();

$eventname = $row['eventname'];
$eventdesc = $row['eventdesc'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DOMELJEN Events Unlimited Inc</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">      
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
	 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->       

</head>
<body class="homepage">   
    <?php include 'navbar.php'; ?>

    <section id="portfolio">
        <div class="container">
            <div class="center">
             <h2><?php echo $eventname; ?></h2>
             <p class="lead"><?php echo $eventdesc; ?></p>
         </div>

         <div class="row">

            <?php 

            include 'admin/config/config.php';
            $admins = $conn->query("SELECT * from tblnewsupdate WHERE `eventid`=".$_GET['eventid']);
            while($r = $admins->fetch()){
                ?>
                    <div class="blog-item">
                        <div class="row">
                            <div class="col-xs-12 col-sm-10 blog-content">
                                <a href="#"><img class="img-responsive img-blog" src="<?php echo $r['picture']; ?>" width="50%" alt="" /></a>
                                <h2><a href="blog-item.html"><?php echo $r['title']; ?></a></h2>
                                <h3><?php echo $r['content']; ?></h3>
                            </div>
                        </div>    
                    </div><!--/.blog-item-->
                <?php
            }
            ?>
        </div>
    </div>
</section><!--/#portfolio-item-->
<?php include 'footer.php'; ?>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/jquery.isotope.min.js"></script>   
<script src="js/wow.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>