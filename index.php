<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DOMELJEN Events Unlimited Inc</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">      
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->       

</head>
<body class="homepage">   
    <?php include 'navbar.php'; ?>
    <section id="services" class="service-item">
       <div class="container">
            <div class="center wow fadeInDown">
                <h2>Welcome to DOMELJEN Events Unlimited Inc</h2>
                <p class="lead">DOMELJEN is a company associated in providing top quality and affordable lights and sounds equipment and other professional services that can help bring your event to the forefront of all events.</p>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/services1.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Scalable</h3>
                            <p>From big to small events, we can cater them all.</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/services2.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">On Time</h3>
                            <p>We always come and prepare on time.</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/services3.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Customizable</h3>
                            <p>Package are customizable and affordable.</p>
                        </div>
                    </div>
                </div>  

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/services4.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Lights &amp Sounds</h3>
                            <p>We have the brightest and loudest sound you can have.</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/services5.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Equiped</h3>
                            <p>We have latest equipment for you event needs.</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/services/services6.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Happy Customers</h3>
                            <p>We do not leave our  customer unsatisfied.</p>
                        </div>
                    </div>
                </div>                                                
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#services-->



<section id="feature" >
    <div class="container">

        <div class="center wow fadeInDown">
            <h2>ONE-STOP SHOP for EVENT NEEDS</h2>
            <p class="lead">We cater different types of events and we assure you that you will get most out of your investment to us. </p>
        </div>

        <div class="row">
            <div class="features">
                <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="feature-wrap">
                        <i class="fa fa-music"></i>
                        <h2>Concerts</h2>
                        <h3>Party Rockin till you drop!</h3>
                    </div>
                </div><!--/.col-md-4-->

                <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="feature-wrap">
                        <i class="fa fa-users"></i>
                        <h2>Government Events </h2>
                        <h3>For government ralated events.</h3>
                    </div>
                </div><!--/.col-md-4-->

                <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="feature-wrap">
                        <i class="fa fa-building"></i>
                        <h2>Company Events</h2>
                        <h3>Company events? No problem!</h3>
                    </div>
                </div><!--/.col-md-4-->
                
                <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="feature-wrap">
                        <i class="fa fa-graduation-cap"></i>
                        <h2>School Events</h2>
                        <h3>Recognition? Graduation? Meetings? </h3>
                    </div>
                </div><!--/.col-md-4-->

                <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="feature-wrap">
                        <i class="fa fa-user"></i>
                        <h2>Religious Events</h2>
                        <h3>Preach! Get heard!</h3>
                    </div>
                </div><!--/.col-md-4-->

                <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="feature-wrap">
                        <i class="fa fa-heart"></i>
                        <h2>Campus Events</h2>
                        <h3>Event ? Campus Party? We have you covered! </h3>
                    </div>
                </div><!--/.col-md-4-->


                <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="feature-wrap">
                        <i class="fa fa-star-o"></i>
                        <h2>Fashion Shows</h2>
                        <h3>Awesome spot lights for models and viewers</h3>
                    </div>
                </div><!--/.col-md-4-->

                <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="feature-wrap">
                        <i class="fa fa-life-ring"></i>
                        <h2>Weddings</h2>
                        <h3>For your new life!</h3>
                    </div>
                </div><!--/.col-md-4-->

                <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="feature-wrap">
                        <i class="fa fa-birthday-cake"></i>
                        <h2>Birthday/Debut</h2>
                        <h3>Step up your Birthday Party!</h3>
                    </div>
                </div><!--/.col-md-4-->
            </div><!--/.services-->
        </div><!--/.row-->    
    </div><!--/.container-->
</section><!--/#feature-->



<section id="feature" class="transparent-bg">
    <div class="container">
        <div class="get-started center wow fadeInDown">
            <h2>Ready to Get Started?</h2>
            <p class="lead">Our mission is to make memorable events more meaningful, constantly strive to meet and exceed customers’ needs and expectations, as taking pride in making the best event possible, WHAT YOU WISH IS WHAT WE GIVE</p>

            <div class="request">
                <h4><a href="#">Reserve Now!</a></h4>
            </div>
        </div><!--/.get-started-->

        <div class="clients-area center wow fadeInDown">
            <h2>What our client says</h2>
            <p class="lead">Not sure to avail our services? Read what our client says! We do not leave our clients unsatisfied.</p>
        </div>

        <div class="row">
            <div class="col-md-4 wow fadeInDown">
                <div class="clients-comments text-center">
                    <img src="images/client1.png" class="img-circle" alt="">
                    <h3>"I love affordable lights and sounds. Affordable lights and sounds has got everything I need. Affordable lights and sounds has really helped our business. We're loving it."</h3>
                    <h4><span>-John Doe /</span>  Director of Campus.com</h4>
                </div>
            </div>
            <div class="col-md-4 wow fadeInDown">
                <div class="clients-comments text-center">
                    <img src="images/client2.png" class="img-circle" alt="">
                    <h3>"Thanks Domeljen Lights and Sounds! Domeljen Lights and Sounds should be nominated for service of the year. The service was excellent."</h3>
                    <h4><span>-John Doe /</span>  Director of Fisher.com</h4>
                </div>
            </div>
            <div class="col-md-4 wow fadeInDown">
                <div class="clients-comments text-center">
                    <img src="images/client3.png" class="img-circle" alt="">
                    <h3>"It really saves me time and effort. Domeljen Lights and Sounds is exactly what our business has been lacking. Domeljen Lights and Sounds is worth much more than I paid. I was amazed at the quality of Domeljen Lights and Sounds."</h3>
                    <h4><span>-John Doe /</span>  Director of Rappy.com</h4>
                </div>
            </div>
        </div>
    </div><!--/.container-->
</section><!--/#feature-->






    <?php include 'footer.php'; ?>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>   
    <script src="js/wow.min.js"></script>
    <script src="js/main.js"></script>
</body>
</html>