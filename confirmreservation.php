<?php 

function secure($str){
	return strip_tags(trim(htmlspecialchars($str)));
}

if(isset($_GET['reservationid']) && isset($_GET['email'])){
	include 'admin/config/config.php';
	$id = secure($_GET['reservationid']);
	$email = secure($_GET['email']);

	$stmt = $conn->prepare("SELECT * FROM `tblreservation` INNER JOIN tblcustomer on tblreservation.custid = tblcustomer.custid WHERE `reservationid`=:id AND tblcustomer.email = :email");
	$stmt->bindParam(':id',$id);
	$stmt->bindParam(':email',$email);
	$stmt->execute(); 
	$row = $stmt->fetch();

	$resid = secure($row['reservationid']);
	$eclick = secure($row['EmailClicked']);

	if($row['EmailClicked'] == "NO"){

		$stmt = $conn->prepare("UPDATE tblreservation set `EmailClicked`='YES', status='Pending' WHERE `reservationid`=:id");
		$stmt->bindParam(':id',$id);
		$stmt->execute(); 
		header("Location: successconfirm.php");
	}else{
		header("Location: reservation.php");
	}
}else{
	header("Location: reservation.php");
}

?>