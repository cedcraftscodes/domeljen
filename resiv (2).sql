-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2018 at 12:03 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `resiv`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbladdons`
--

CREATE TABLE `tbladdons` (
  `addonid` int(11) NOT NULL,
  `addonname` varchar(255) NOT NULL,
  `price` double(18,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbladdons`
--

INSERT INTO `tbladdons` (`addonid`, `addonname`, `price`) VALUES
(1, 'BELTPACK HEADSET STARDJS', 1000.00),
(3, 'PROFESSIONAL BAND SET-UP (RHYTHM SECTION)', 10000.00),
(4, 'COMMUNICATION SET', 5000.00),
(5, 'COMPLETE PANEL SYSTEM', 30.00),
(6, 'WIDE SCREEN PROJECTOR', 8000.00),
(7, 'LIVE VIDEO COVERAGE (final video copy on DVD format)', 30000.00),
(8, '100 KVA GENERATOR SET WITH 8 HOURS FUEL', 13000.00),
(9, '125 KVA GENERATOR SET WITH 8 HOURS FUEL', 16000.00),
(10, '150 KVA GENERATOR SET WITH 8 HOURS FUEL', 20000.00),
(11, 'STAGE', 12000.00),
(12, 'ROOFING 1', 15000.00),
(13, 'ROOFING 2', 25000.00),
(14, 'ROOFING 3', 35000.00),
(15, 'TENT', 1500.00),
(16, 'EMCEE / HOST', 5000.00),
(17, '3 PC BAND', 6000.00),
(18, '5 PC BAND', 8000.00),
(19, '7 PC BAND', 15000.00);

-- --------------------------------------------------------

--
-- Table structure for table `tbladdons_equip`
--

CREATE TABLE `tbladdons_equip` (
  `addeqid` int(11) NOT NULL,
  `eqid` int(11) NOT NULL,
  `addonid` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbladdons_equip`
--

INSERT INTO `tbladdons_equip` (`addeqid`, `eqid`, `addonid`, `quantity`) VALUES
(7, 58, 3, 1),
(8, 52, 3, 1),
(9, 57, 3, 1),
(10, 54, 3, 1),
(11, 53, 3, 1),
(12, 51, 3, 1),
(13, 56, 3, 1),
(14, 55, 3, 1),
(15, 60, 4, 1),
(16, 61, 4, 4),
(17, 62, 4, 5),
(18, 63, 5, 1),
(19, 64, 6, 1),
(20, 68, 7, 1),
(21, 67, 7, 1),
(22, 65, 7, 2),
(23, 66, 7, 2),
(24, 69, 8, 1),
(25, 70, 9, 1),
(26, 71, 10, 1),
(27, 72, 11, 1),
(28, 73, 12, 1),
(29, 74, 13, 1),
(30, 75, 14, 1),
(31, 76, 15, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbladmins`
--

CREATE TABLE `tbladmins` (
  `userid` int(11) NOT NULL,
  `fname` varchar(25) NOT NULL,
  `mname` varchar(25) NOT NULL,
  `lname` varchar(25) NOT NULL,
  `user_name` varchar(55) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `birthday` varchar(20) NOT NULL,
  `pass_word` varchar(55) NOT NULL,
  `acctype` enum('cashier','admin','','') NOT NULL,
  `deleted` enum('YES','NO','','') NOT NULL,
  `accimg` varchar(255) NOT NULL,
  `dateadded` int(11) NOT NULL,
  `passchange` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbladmins`
--

INSERT INTO `tbladmins` (`userid`, `fname`, `mname`, `lname`, `user_name`, `gender`, `birthday`, `pass_word`, `acctype`, `deleted`, `accimg`, `dateadded`, `passchange`) VALUES
(56, 'Admin', 'Admin', 'Admin', 'AAAdmin926', 'male', '1998-12-02', '304a6f11db2deb8086be81b8b6266c7c', 'admin', 'NO', 'images/dp/AAAdmin926_632479.png', 1516799582, '1');

-- --------------------------------------------------------

--
-- Table structure for table `tblcompanyinfo`
--

CREATE TABLE `tblcompanyinfo` (
  `SID` int(11) NOT NULL,
  `settingkey` varchar(255) NOT NULL,
  `settingvalue` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcompanyinfo`
--

INSERT INTO `tblcompanyinfo` (`SID`, `settingkey`, `settingvalue`) VALUES
(1, 'name', 'Giga Ohms'),
(2, 'receiver', 'Nick Catimbang'),
(3, 'street', 'Km. 36 National Highway Bgy. Platero'),
(4, 'city', ' BiÃ±an City'),
(5, 'province', 'Laguna'),
(6, 'zipcode', '4024'),
(7, 'phone', '09228335230'),
(8, 'email', 'icorrelate@gmail.com'),
(9, 'voidcode', '244161004191'),
(10, 'tin', '412-252-526');

-- --------------------------------------------------------

--
-- Table structure for table `tblcustomer`
--

CREATE TABLE `tblcustomer` (
  `custid` int(11) NOT NULL,
  `first_name` varchar(55) NOT NULL,
  `last_name` varchar(55) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(55) NOT NULL,
  `state` varchar(55) NOT NULL,
  `zip` int(11) NOT NULL,
  `title` varchar(55) NOT NULL,
  `company` varchar(100) NOT NULL,
  `phone` varchar(55) NOT NULL,
  `email` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcustomer`
--

INSERT INTO `tblcustomer` (`custid`, `first_name`, `last_name`, `address`, `city`, `state`, `zip`, `title`, `company`, `phone`, `email`) VALUES
(17, '', '', '', '', '', 0, '', '', '', 'icorrelate@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tblequipments`
--

CREATE TABLE `tblequipments` (
  `eqid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblequipments`
--

INSERT INTO `tblequipments` (`eqid`, `name`, `count`) VALUES
(9, 'KEVLER PRX 615 SPEAKER WITH STAND', 48),
(10, 'YAMAHA MG16 CHANNEL MIXER', 49),
(11, 'SAMSON DYNAMIC CORDED MICROPHONES WITH STAND', 48),
(12, 'TOSHIBA LAPTOP', 49),
(13, 'CABLES &amp; CONNECTORS', 48),
(14, 'EXTENSION CORD', 48),
(15, 'JBL SRX SPEAKER 2200W', 50),
(19, 'KEVLER VZ500 AMPLIFIER', 50),
(20, 'KEVLER MC2 AMPLIFIER', 50),
(21, 'KEVLER CROSSOVER', 50),
(22, 'KEVLER GRAPHIC EQUALIZER', 50),
(24, 'YAMAHA MG32 CHANNEL MIXER', 50),
(25, 'SHURE SVX WIRELESS MICROPHONE', 50),
(26, 'PIONEER CDJ-350 PROFESSIONAL CD PLAYER', 50),
(27, 'PIONEER DJM-350 (DJ MIXER)', 50),
(28, 'KEVLER PRX 615 SPEAKER MONITOR', 50),
(29, 'LEXUS DUAL 18 SUBWOOFER', 50),
(30, 'EON JBL SPEAKER', 50),
(31, 'MASCOT WIRELESS MICROPHONE', 50),
(32, 'SCAFFOLDING', 50),
(33, 'MICROPHONE STAND', 50),
(34, 'MAIN LINE', 49),
(35, 'LED LIGHT 3W', 42),
(36, 'PAR 64 WITH STAND', 46),
(37, 'MIRROR BALL', 49),
(38, 'DMX LIGHT CONTROLLER', 50),
(39, 'PROFILE 575 MOVING HEAD', 50),
(40, 'VERTICAL SMOKE MACHINE', 50),
(41, 'ANTARI HAZE MACHINE', 50),
(42, 'INVISIBLE/VISIBLE LASER', 50),
(43, 'FOLLOW SPOTLIGHT HMI 1200', 50),
(44, 'LED 54 BULBS RGBW', 50),
(45, 'AVOLITES PEARL 2010  MOVING LIGHT CONTROLLER', 50),
(46, 'SHARPY MOVING HEAD', 50),
(47, 'BEAM 200 MOVING HEAD', 50),
(48, 'HURRICANE TWIN BALL', 48),
(49, '1000 WATTS STROBE LIGHT', 50),
(50, 'DMX 512 MOVING LIGHT CONTROLLER', 49),
(51, 'PEARL FORUM DRUM SET with ZILJAN CYMBALS', 50),
(52, 'GALLIEN KRUGGER BASS AMPLIFIER', 50),
(53, 'MARSHALL GUITAR AMPLIFIER MG-100', 50),
(54, 'LANEY GUITAR AMPLIFIER', 50),
(55, 'SHURE DRUM KIT', 50),
(56, 'PEAVEY KB4 KEYBOARD AMPLIFIER', 50),
(57, 'KEYBOARD STAND', 50),
(58, 'ATRIL', 50),
(59, 'BELTPACK HEADSET STARDJâ€™S', 50),
(60, 'COMMUNICATION BASE', 49),
(61, 'COMMUNICATION BELT', 46),
(62, 'COMMUNICATION HEADSET', 45),
(63, 'LED VIDEO WALL 9ft X 12ft', 49),
(64, 'LCD WIDE SCREEN PROJECTOR 9ft X 12ft', 50),
(65, 'CAMERA VX 2000 SONY', 50),
(66, 'TRIPOD', 50),
(67, 'CAMERA SYSTEM', 50),
(68, 'AUDIO / VIDEO CABLES', 50),
(69, '100 KVA GENERATOR SET WITH 8 HOURS FUEL', 49),
(70, '125 KVA GENERATOR SET WITH 8 HOURS FUEL', 50),
(71, '150 KVA GENERATOR SET WITH 8 HOURS FUEL', 50),
(72, 'STAGE - 16ft X 24ft X 4ft WITH 8ft X 16ft CATWALK', 50),
(73, 'ROOFING â€“ 16ft X 24ft', 50),
(74, 'ROOFING â€“ 20ft X 30ft', 50),
(75, 'ROOFING â€“ 40ft X 40ft', 50),
(76, 'TENT 10ft X 15ft', 50),
(77, 'ROUND TABLE , SQUARE TABLE, LONG TABLE WITH CLOTH', 50),
(78, 'CHAIRS WITH CLOTH', 50);

-- --------------------------------------------------------

--
-- Table structure for table `tblevents`
--

CREATE TABLE `tblevents` (
  `eventid` int(11) NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `eventdesc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblevents`
--

INSERT INTO `tblevents` (`eventid`, `eventname`, `eventdesc`) VALUES
(1, 'Birthday', 'Birthday');

-- --------------------------------------------------------

--
-- Table structure for table `tbllogins`
--

CREATE TABLE `tbllogins` (
  `LoginID` int(11) NOT NULL,
  `AccountID` int(11) NOT NULL,
  `LoginDate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbllogins`
--

INSERT INTO `tbllogins` (`LoginID`, `AccountID`, `LoginDate`) VALUES
(51, 22, 1516584258),
(52, 22, 1516627676),
(53, 22, 1516663143),
(54, 22, 1516693884),
(55, 22, 1516717581),
(56, 22, 1516722709),
(57, 22, 1516793582),
(58, 54, 1516793618),
(59, 54, 1516793827),
(60, 55, 1516793877),
(61, 22, 1516794171),
(62, 22, 1516795416),
(63, 22, 1516799515),
(64, 56, 1516799590);

-- --------------------------------------------------------

--
-- Table structure for table `tblnewsupdate`
--

CREATE TABLE `tblnewsupdate` (
  `newsid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `picture` varchar(255) NOT NULL,
  `eventid` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblpackages`
--

CREATE TABLE `tblpackages` (
  `packid` int(11) NOT NULL,
  `packname` varchar(200) NOT NULL,
  `packagetype` varchar(50) NOT NULL,
  `noOfCrew` int(11) NOT NULL,
  `Price` double(18,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblpackages`
--

INSERT INTO `tblpackages` (`packid`, `packname`, `packagetype`, `noOfCrew`, `Price`) VALUES
(0, 'NA', 'NA', 0, 0.00),
(6, 'Package 1', 'sound', 2, 3500.00),
(7, 'Package 2', 'sound', 2, 6000.00),
(8, 'Package 3', 'sound', 3, 8500.00),
(9, 'Package 4', 'sound', 4, 12000.00),
(10, 'Package 1', 'lights', 1, 2500.00),
(11, 'PAckage 2', 'lights', 1, 5000.00),
(12, 'Package 3', 'lights', 2, 8000.00),
(13, 'Package 4', 'lights', 4, 12000.00);

-- --------------------------------------------------------

--
-- Table structure for table `tblpackage_equip`
--

CREATE TABLE `tblpackage_equip` (
  `pckeq` int(11) NOT NULL,
  `eqid` int(11) NOT NULL,
  `packid` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblpackage_equip`
--

INSERT INTO `tblpackage_equip` (`pckeq`, `eqid`, `packid`, `quantity`) VALUES
(6, 2, 3, 5),
(7, 4, 3, 4),
(8, 5, 3, 3),
(9, 6, 3, 3),
(10, 2, 4, 5),
(11, 4, 4, 4),
(22, 4, 5, 6),
(23, 5, 5, 6),
(24, 9, 6, 2),
(25, 10, 6, 1),
(26, 11, 6, 2),
(27, 12, 6, 1),
(28, 13, 6, 1),
(29, 14, 6, 1),
(30, 9, 7, 4),
(31, 10, 7, 1),
(32, 11, 7, 4),
(33, 12, 7, 1),
(34, 13, 7, 1),
(35, 14, 7, 1),
(36, 15, 7, 2),
(37, 19, 7, 1),
(38, 20, 7, 1),
(39, 21, 7, 1),
(40, 22, 7, 1),
(41, 9, 8, 8),
(42, 10, 8, 1),
(43, 11, 8, 4),
(44, 12, 8, 1),
(45, 13, 8, 1),
(46, 14, 8, 1),
(47, 15, 8, 2),
(48, 19, 8, 1),
(49, 20, 8, 1),
(50, 21, 8, 1),
(51, 22, 8, 1),
(52, 25, 8, 2),
(53, 26, 8, 2),
(54, 27, 8, 1),
(55, 13, 9, 1),
(56, 30, 9, 4),
(57, 14, 9, 1),
(58, 15, 9, 4),
(59, 21, 9, 1),
(60, 22, 9, 2),
(61, 20, 9, 2),
(62, 9, 9, 8),
(63, 19, 9, 2),
(64, 29, 9, 4),
(65, 34, 9, 2),
(66, 31, 9, 2),
(67, 33, 9, 1),
(68, 26, 9, 2),
(69, 27, 9, 1),
(70, 11, 9, 4),
(71, 32, 9, 2),
(72, 25, 9, 2),
(73, 12, 9, 2),
(74, 10, 9, 1),
(75, 24, 9, 1),
(76, 13, 10, 1),
(77, 50, 10, 1),
(78, 14, 10, 1),
(79, 48, 10, 2),
(80, 35, 10, 8),
(81, 34, 10, 1),
(82, 37, 10, 1),
(83, 36, 10, 4),
(84, 41, 11, 1),
(85, 13, 11, 1),
(86, 38, 11, 2),
(87, 14, 11, 1),
(88, 43, 11, 1),
(89, 42, 11, 1),
(90, 35, 11, 8),
(91, 34, 11, 1),
(92, 37, 11, 1),
(93, 36, 11, 8),
(94, 39, 11, 4),
(95, 40, 11, 1),
(96, 41, 12, 1),
(97, 45, 12, 1),
(98, 13, 12, 1),
(99, 14, 12, 1),
(100, 43, 12, 1),
(101, 42, 12, 1),
(102, 44, 12, 8),
(103, 35, 12, 8),
(104, 34, 12, 1),
(105, 37, 12, 1),
(106, 36, 12, 8),
(107, 39, 12, 4),
(108, 46, 12, 2),
(109, 40, 12, 2),
(110, 49, 13, 1),
(111, 41, 13, 1),
(112, 45, 13, 1),
(113, 47, 13, 2),
(114, 13, 13, 1),
(115, 50, 13, 4),
(116, 14, 13, 1),
(117, 43, 13, 1),
(118, 48, 13, 2),
(119, 42, 13, 1),
(120, 44, 13, 8),
(121, 35, 13, 8),
(122, 34, 13, 1),
(123, 37, 13, 1),
(124, 36, 13, 16),
(125, 39, 13, 6),
(126, 46, 13, 4),
(127, 40, 13, 2),
(156, 69, 2, 10),
(157, 49, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tblpromos`
--

CREATE TABLE `tblpromos` (
  `promoid` int(11) NOT NULL,
  `promocode` varchar(50) NOT NULL,
  `discount` double(18,2) NOT NULL,
  `minpurchase` double(18,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblpromos`
--

INSERT INTO `tblpromos` (`promoid`, `promocode`, `discount`, `minpurchase`) VALUES
(2, 'JANUARYLIGHTS2018', 2333.00, 10000.00),
(4, 'WHITENEWYEAR', 200.00, 1000.00);

-- --------------------------------------------------------

--
-- Table structure for table `tblreservation`
--

CREATE TABLE `tblreservation` (
  `reservationid` int(11) NOT NULL,
  `custid` int(11) NOT NULL,
  `eventdate` date NOT NULL,
  `status` varchar(55) NOT NULL,
  `EmailClicked` enum('YES','NO') NOT NULL DEFAULT 'NO',
  `eventtype` varchar(55) NOT NULL,
  `couponcode` varchar(55) NOT NULL,
  `discount` double(18,2) NOT NULL,
  `total` double(18,2) NOT NULL,
  `lightpack` int(11) NOT NULL,
  `soundpack` int(11) NOT NULL,
  `datesent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblreservation`
--

INSERT INTO `tblreservation` (`reservationid`, `custid`, `eventdate`, `status`, `EmailClicked`, `eventtype`, `couponcode`, `discount`, `total`, `lightpack`, `soundpack`, `datesent`) VALUES
(9, 17, '2018-01-25', 'Pending', 'YES', 'School Events', 'WHITENEWYEAR', 200.00, 31830.00, 10, 6, 1516780929);

-- --------------------------------------------------------

--
-- Table structure for table `tblreservation_addons`
--

CREATE TABLE `tblreservation_addons` (
  `reservationid` int(11) NOT NULL,
  `addonid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblreservation_addons`
--

INSERT INTO `tblreservation_addons` (`reservationid`, `addonid`) VALUES
(1, 8),
(1, 9),
(1, 10),
(2, 8),
(2, 9),
(2, 10),
(3, 8),
(3, 9),
(3, 10),
(4, 8),
(4, 9),
(4, 10),
(5, 8),
(5, 9),
(5, 10),
(7, 8),
(7, 9),
(7, 10),
(8, 8),
(8, 9),
(8, 10),
(9, 8),
(9, 18),
(9, 4),
(9, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tblwrongtries`
--

CREATE TABLE `tblwrongtries` (
  `id` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblwrongtries`
--

INSERT INTO `tblwrongtries` (`id`, `time`, `type`) VALUES
(1, 1506082714, 'cashier'),
(2, 1506086282, 'cashier');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbladdons`
--
ALTER TABLE `tbladdons`
  ADD PRIMARY KEY (`addonid`);

--
-- Indexes for table `tbladdons_equip`
--
ALTER TABLE `tbladdons_equip`
  ADD PRIMARY KEY (`addeqid`);

--
-- Indexes for table `tbladmins`
--
ALTER TABLE `tbladmins`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `tblcompanyinfo`
--
ALTER TABLE `tblcompanyinfo`
  ADD PRIMARY KEY (`SID`);

--
-- Indexes for table `tblcustomer`
--
ALTER TABLE `tblcustomer`
  ADD PRIMARY KEY (`custid`);

--
-- Indexes for table `tblequipments`
--
ALTER TABLE `tblequipments`
  ADD PRIMARY KEY (`eqid`);

--
-- Indexes for table `tblevents`
--
ALTER TABLE `tblevents`
  ADD PRIMARY KEY (`eventid`);

--
-- Indexes for table `tbllogins`
--
ALTER TABLE `tbllogins`
  ADD PRIMARY KEY (`LoginID`);

--
-- Indexes for table `tblnewsupdate`
--
ALTER TABLE `tblnewsupdate`
  ADD PRIMARY KEY (`newsid`);

--
-- Indexes for table `tblpackages`
--
ALTER TABLE `tblpackages`
  ADD PRIMARY KEY (`packid`);

--
-- Indexes for table `tblpackage_equip`
--
ALTER TABLE `tblpackage_equip`
  ADD PRIMARY KEY (`pckeq`);

--
-- Indexes for table `tblpromos`
--
ALTER TABLE `tblpromos`
  ADD PRIMARY KEY (`promoid`);

--
-- Indexes for table `tblreservation`
--
ALTER TABLE `tblreservation`
  ADD PRIMARY KEY (`reservationid`);

--
-- Indexes for table `tblwrongtries`
--
ALTER TABLE `tblwrongtries`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbladdons`
--
ALTER TABLE `tbladdons`
  MODIFY `addonid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tbladdons_equip`
--
ALTER TABLE `tbladdons_equip`
  MODIFY `addeqid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tbladmins`
--
ALTER TABLE `tbladmins`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `tblcompanyinfo`
--
ALTER TABLE `tblcompanyinfo`
  MODIFY `SID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tblcustomer`
--
ALTER TABLE `tblcustomer`
  MODIFY `custid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tblequipments`
--
ALTER TABLE `tblequipments`
  MODIFY `eqid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `tblevents`
--
ALTER TABLE `tblevents`
  MODIFY `eventid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbllogins`
--
ALTER TABLE `tbllogins`
  MODIFY `LoginID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `tblnewsupdate`
--
ALTER TABLE `tblnewsupdate`
  MODIFY `newsid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblpackages`
--
ALTER TABLE `tblpackages`
  MODIFY `packid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblpackage_equip`
--
ALTER TABLE `tblpackage_equip`
  MODIFY `pckeq` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;

--
-- AUTO_INCREMENT for table `tblpromos`
--
ALTER TABLE `tblpromos`
  MODIFY `promoid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tblreservation`
--
ALTER TABLE `tblreservation`
  MODIFY `reservationid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tblwrongtries`
--
ALTER TABLE `tblwrongtries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
