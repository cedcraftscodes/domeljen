	<header id="header">
		<nav class="navbar navbar-fixed-top" role="banner">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php">DOMELJEN</a>
				</div>
				
				<div class="collapse navbar-collapse navbar-right">
					<ul class="nav navbar-nav">
						<li <?php if( basename($_SERVER['PHP_SELF']) == "index.php") {echo "class='active'"; }?> ><a href="index.php">Home</a></li>
						<li <?php if( basename($_SERVER['PHP_SELF']) == "about-us.php") {echo "class='active'"; }?>><a href="about-us.php">About Us</a></li>
						<li <?php if( basename($_SERVER['PHP_SELF']) == "promos.php") {echo "class='active'"; }?>><a href="promos.php">Promos</a></li>
						<li <?php if( basename($_SERVER['PHP_SELF']) == "gallery.php") {echo "class='active'"; }?>><a href="gallery.php">Gallery</a></li>       
						<li <?php if( basename($_SERVER['PHP_SELF']) == "reservation.php") {echo "class='active'"; }?>><a href="reservation.php">Reserve Now</a></li>                                  
					</ul>
				</div>
			</div><!--/.container-->
		</nav><!--/nav-->
	</header><!--/header-->