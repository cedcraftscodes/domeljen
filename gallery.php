<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DOMELJEN Events Unlimited Inc</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">      
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
	 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->       

</head>
<body class="homepage">   
    <?php include 'navbar.php'; ?>

    <section id="portfolio">
        <div class="container">
            <div class="center">
             <h2>Gallery</h2>
             <p class="lead">Take a peak on what you will get when you avail our services. </p>
         </div>

         <div class="row">

            <div class="portfolio-items">
                <?php 

                include 'admin/config/config.php';

                $admins = $conn->query("SELECT * FROM `tblevents`");

                while($r = $admins->fetch()){

                    $stmt = $conn->prepare("SELECT * from tblnewsupdate WHERE `eventid`=:id LIMIT 1");
                    $stmt->bindParam(':id',$r['eventid']);
                    $stmt->execute(); 
                    $row = $stmt->fetch();

                    $picture = $row['picture'];

                    ?>



                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" width="300px" height="300px" src="<?php echo $picture; ?>" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="showpics.php?eventid=<?php echo $r['eventid']; ?>"><?php echo $r['eventname']; ?></a></h3>
                                    <p><?php echo $r['eventdesc']; ?></p>
                                    <a class="preview" href="<?php echo $picture; ?>" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</section><!--/#portfolio-item-->
<?php include 'footer.php'; ?>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/jquery.isotope.min.js"></script>   
<script src="js/wow.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>