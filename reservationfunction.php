<?php 


if(session_id()){}else{session_start();}

if(isset($_POST['action']) && !empty($_POST['action']))
{

	$action = $_POST['action'];
	switch ($action) {


		case 'showLightInfo':
		showLightInfo();
		break;

		case 'computeTotal':
		computeTotal();
		break;

		case 'reserveNow':
		reserveNow();
		break;
		default:

				# code...
		break;
	}
}

function reserveNow(){
	$messageRow = "";

	$firstname = secure($_POST['firstname']);
	$lastname = secure($_POST['lastname']);
	$address = secure($_POST['address']);
	$city = secure($_POST['city']);
	$state = secure($_POST['state']);
	$zip = secure($_POST['zip']);
	$title = secure($_POST['title']);
	$company = secure($_POST['company']);
	$phone = secure($_POST['phone']);
	$email = secure($_POST['email']);
	//$addons = $_POST['addons'];
	$eventdate = secure($_POST['eventdate']);
	$eventtype = secure($_POST['eventtype']);
	$cboaudiopack = secure($_POST['cboaudiopack']);
	$cbolightspack = secure($_POST['cbolightspack']);
	$coupon = secure($_POST['coupon']);


	//insert customer info
	
	include 'admin/config/config.php';
	$stmt = $conn->prepare("INSERT INTO `tblcustomer`(`first_name`, `last_name`, `address`, `city`, `state`, `zip`, `title`, `company`, `phone`, `email`) VALUES (:fname, :lname, :addr, :city, :state, :zip, :title, :company, :phone, :email)");

	$stmt->bindParam(':fname',$firstname);
	$stmt->bindParam(':lname',$lastname);
	$stmt->bindParam(':addr',$address);
	$stmt->bindParam(':city',$city);
	$stmt->bindParam(':state',$state);
	$stmt->bindParam(':zip',$zip);
	$stmt->bindParam(':title',$title);
	$stmt->bindParam(':company',$company);
	$stmt->bindParam(':phone',$phone);
	$stmt->bindParam(':email',$email);
	$stmt->execute(); 
	$custid = $conn->lastInsertId();
	
	//get id of last inserted user
	
	
	//Compute total amount. 
	$total = 0;

	$stmt = $conn->prepare("SELECT * FROM `tblpackages` WHERE `packid`=:id");
	$stmt->bindParam(':id',$cboaudiopack);
	$stmt->execute(); 
	$row = $stmt->fetch();

	$total += secure($row['Price']);

	if($cboaudiopack != 0){
		$messageRow .= 
		"<tr>
		<td style='padding: 8px;text-align: left;border-bottom: 1px solid #ddd;'>".$row['packname']."'</td>
		<td style='padding: 8px;text-align: left;border-bottom: 1px solid #ddd;'>".$row['Price']."</td>
		</tr>";
	}


	$stmt = $conn->prepare("SELECT * FROM `tblpackages` WHERE `packid`=:id");
	$stmt->bindParam(':id',$cbolightspack);
	$stmt->execute(); 
	$row = $stmt->fetch();

	$total += secure($row['Price']);

	if($cbolightspack != 0){
		$messageRow .= 
		"<tr>
		<td style='padding: 8px;text-align: left;border-bottom: 1px solid #ddd;'>".$row['packname']."'</td>
		<td style='padding: 8px;text-align: left;border-bottom: 1px solid #ddd;'>".$row['Price']."</td>
		</tr>";
	}

	if(isset($_POST['addons'])){
		$addons = $_POST['addons'];
		foreach ($_POST['addons'] as $key) {
			$stmt = $conn->prepare("SELECT * FROM `tbladdons` WHERE `addonid`=:id");
			$stmt->bindParam(':id',$key);
			$stmt->execute(); 
			$row = $stmt->fetch();

			$total += secure($row['price']);

			$messageRow .= 
			"<tr>
			<td style='padding: 8px;text-align: left;border-bottom: 1px solid #ddd;'>".$row['addonname']."'</td>
			<td style='padding: 8px;text-align: left;border-bottom: 1px solid #ddd;'>".$row['price']."</td>
			</tr>";

		}
	}


	$discount = 0;


	$stmt = $conn->prepare("SELECT discount, minpurchase FROM `tblpromos` WHERE `promocode`=:id");
	$stmt->bindParam(':id',$coupon);
	$stmt->execute(); 
	$row = $stmt->fetch();
	$minpurchase = secure($row['minpurchase']);

	if($total >= $minpurchase){
		$total -= secure($row['discount']);
		$discount = ($row['discount'] != null) ? $row['discount'] : 0;

		$messageRow .= 
		"<tr>
		<td style='padding: 8px;text-align: left;border-bottom: 1px solid #ddd;'>Discount</td>
		<td style='padding: 8px;text-align: left;border-bottom: 1px solid #ddd;'>".$discount."</td>
		</tr>";
	}
	$messageRow .= 
	"<tr>
	<td style='padding: 8px;text-align: left;border-bottom: 1px solid #ddd;'>Total</td>
	<td style='padding: 8px;text-align: left;border-bottom: 1px solid #ddd;'>".$total."</td>
	</tr>";

	//get audio pack equipments and diminish them based on package description. 
	
	$audioequip = $conn->query(
		"SELECT peq.`quantity`, eq.name,eq.eqid  FROM `tblpackage_equip` peq
		INNER JOIN tblequipments as eq 
		ON eq.eqid = peq.`eqid`
		WHERE packid=".$cboaudiopack);

	while($r = $audioequip->fetch()){
		//Diminish; 
		$stmt = $conn->prepare("UPDATE `tblequipments` SET `count`=`count` - :ct WHERE `eqid`=:eqid");
		$stmt->bindParam(':ct', $r['quantity'], PDO::PARAM_INT);
		$stmt->bindParam(':eqid',$r['eqid']);
		$stmt->execute(); 
		
	}
	
	//get lights pack equipments and diminish them based on package description. 
	$lightspack = $conn->query(
		"SELECT peq.`quantity`, eq.name,eq.eqid  FROM `tblpackage_equip` peq
		INNER JOIN tblequipments as eq 
		ON eq.eqid = peq.`eqid`
		WHERE packid=".$cbolightspack);

	while($r = $lightspack->fetch()){
		//Diminish; 
		$stmt = $conn->prepare("UPDATE `tblequipments` SET `count`=`count` - :ct WHERE `eqid`=:eqid");
		$stmt->bindParam(':ct', $r['quantity'], PDO::PARAM_INT);
		$stmt->bindParam(':eqid',$r['eqid']);
		$stmt->execute(); 
		
	}
	$time = time();

	//insert reservation to database
	include 'admin/config/config.php';
	$stmt = $conn->prepare("INSERT INTO `tblreservation`(`custid`, `eventdate`, `status`, `eventtype`, `couponcode`, `discount`, `total`, `lightpack`, `soundpack`, `datesent`) VALUES (:custid, :eventdate, :status , :eventtype, :couponcode, :discount, :total, :lighpack, :soundpack, :datesent )");

	$status = 'Pending Verification';
	$stmt->bindParam(':custid',$custid);
	$stmt->bindParam(':eventdate',$eventdate);
	$stmt->bindParam(':status',$status);
	$stmt->bindParam(':eventtype',$eventtype);
	$stmt->bindParam(':couponcode',$coupon);
	$stmt->bindParam(':discount',$discount);
	$stmt->bindParam(':total',$total);
	$stmt->bindParam(':lighpack',$cbolightspack);
	$stmt->bindParam(':soundpack',$cboaudiopack);
	$stmt->bindParam(':datesent',$time);
	$stmt->execute(); 
	$resid = $conn->lastInsertId();
	
	

	if(isset($_POST['addons'])){
		$addons = $_POST['addons'];
		foreach ($_POST['addons'] as $key) {

			//insert addons to reservation_addons
			$stmt = $conn->prepare("INSERT INTO `tblreservation_addons`(`reservationid`, `addonid`) VALUES (:resid, :addid)");
			$stmt->bindParam(':resid',$resid);
			$stmt->bindParam(':addid',$key);
			$stmt->execute(); 

			//diminish items to equipments inventory
			$addon = $conn->query(
				"SELECT peq.`quantity`, eq.name, eq.eqid  FROM `tbladdons_equip` as peq
				INNER JOIN tblequipments as eq 
				ON eq.eqid = peq.`eqid`
				WHERE addonid=".$key);

			while($r = $addon->fetch()){
					//Diminish; 
				$stmt = $conn->prepare("UPDATE `tblequipments` SET `count`=`count` - :ct WHERE `eqid`=:eqid");
				$stmt->bindParam(':ct', $r['quantity'], PDO::PARAM_INT);
				$stmt->bindParam(':eqid',$r['eqid']);
				$stmt->execute(); 
			}
		}
	}

	//email customer 
	
	
	$to = $email;
	$subject = "Confirm Reservation";

	$message = "<!DOCTYPE html>
	<html>
	<head>
	<title></title>
	</head>
	<body>
	<center><h1>DOMELJEN Events Unlimited Inc</h1>
	<hr >
	<h2>Reservation Details</h2>
	
	</center>
	<p>Name: ".$firstname." ".$lastname."</p>
	<p>Event Type: $eventtype</p>
	<p>Event Date: $eventdate</p>
	<center>
	<table style='border-collapse: collapse;width: 600px;'>";

	$message .= $messageRow;

	$message .= "

	</table>

	<br/>

	<a href='http://veecms.xyz/domeljen/confirmreservation.php?reservationid=".$resid."&email=".$email."' style='
	background-color: #4DBFF4; /* Green */
	border: none;
	color: white;
	padding: 15px 32px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 16px;' >Click here to CONFIRM RESERVATION</a>
	</center>

	</body>
	</html>
	";

	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

	// More headers
	$headers .= 'From: <domeljen@veecms.xyz>' . "\r\n";
	mail($to,$subject,$message,$headers);

	echo "success";
	

}
function computeTotal(){
	include 'admin/config/config.php';

	$total = 0;
	$spackid = secure($_POST['spackid']);
	$apackid = secure($_POST['apackid']);

	$stmt = $conn->prepare("SELECT * FROM `tblpackages` WHERE `packid`=:id");
	$stmt->bindParam(':id',$spackid);
	$stmt->execute(); 
	$row = $stmt->fetch();

	$total += secure($row['Price']);


	$stmt = $conn->prepare("SELECT * FROM `tblpackages` WHERE `packid`=:id");
	$stmt->bindParam(':id',$apackid);
	$stmt->execute(); 
	$row = $stmt->fetch();

	$total += secure($row['Price']);

	if(isset($_POST['addons'])){
		$addons = $_POST['addons'];
		foreach ($_POST['addons'] as $key) {
			$stmt = $conn->prepare("SELECT * FROM `tbladdons` WHERE `addonid`=:id");
			$stmt->bindParam(':id',$key);
			$stmt->execute(); 
			$row = $stmt->fetch();

			$total += secure($row['price']);
		}
	}


	$promocode = $_POST['promocode'];
	$stmt = $conn->prepare("SELECT discount, minpurchase FROM `tblpromos` WHERE `promocode`=:id");
	$stmt->bindParam(':id',$promocode);
	$stmt->execute(); 
	$row = $stmt->fetch();
	$minpurchase = secure($row['minpurchase']);

	if($total >= $minpurchase){
		$total -= secure($row['discount']);
	}


	echo json_encode(array(
		"total" => $total
	));

}
function showLightInfo(){
	$packid = (int)secure($_POST['packid']);

	include 'admin/config/config.php';

	$admins = $conn->query(
		"SELECT peq.`quantity`, eq.name  FROM `tblpackage_equip` peq
		INNER JOIN tblequipments as eq 
		ON eq.eqid = peq.`eqid`
		WHERE packid=".$packid);

	while($r = $admins->fetch()){
		echo "<p>".$r['quantity']." ".$r['name']."</p>";
	}

	$stmt = $conn->prepare("SELECT * FROM `tblpackages` WHERE `packid`=:id");
	$stmt->bindParam(':id',$packid);
	$stmt->execute(); 
	$row = $stmt->fetch();

	echo "<strong><p class='pull-right'>PHP ".$row['Price']."</p></strong>";

}

function secure($str){
	return strip_tags(trim(htmlspecialchars($str)));
}


function ContainsNumbers($String){
	return preg_match('/\\d/', $String) > 0;
}

function addEquipment()
{
	include '../config/mconfig.php';

	/*
		Validations 
	*/
		$errors = array();

		/*

		if(strlen($_POST['fname']) == 0){
			array_push($errors, "First Name can not be blank!");
		}else{
			if(ContainsNumbers($_POST['fname'])){
				array_push($errors, "First Name contains number!");
			}
		}
		*/

		
		if(count($errors) > 0 )
		{

			echo "<tr><script type='text/javascript'>
			$(document).ready(function(){
				$('#msgtitle').text('Error');
				$('#modalmsg').html(\"".implode("<br />",$errors)."\");
				$('#msgmodalbtn').text('Close');
				$('#msgmodalbtn').attr('class', 'btn btn-danger pull-right');
				$('#msgmodalheader').attr('class', 'modal-header modal-header-danger');
				$('#msgmodal').modal('show');



			});
			</script></tr>";
		}
		else
		{


			$eqname = secure($_POST['eqname']);
			$quantity = secure($_POST['quantity']);

		// prepare and bind
			$stmt = $conn->prepare("INSERT INTO `tblequipments`( `name`, `count`)  VALUES (?, ?)");
			$stmt->bind_param("ss", $eqname, $quantity);
			$stmt->execute();



			echo "<tr><script type='text/javascript'>
			$(document).ready(function(){
				$('#msgtitle').text('Success');
				$('#modalmsg').html('Equipment successfully added!');
				$('#msgmodalbtn').text('Close');
				$('#msgmodalbtn').attr('class', 'btn btn-success pull-right');
				$('#msgmodalheader').attr('class', 'modal-header modal-header-success');
				$('#msgmodal').modal('show');
			});
			</script></tr>";
		}
		showAdmins();


	}




	function showAdmins()
	{
		include '../config/config.php';

		$admins = $conn->query("SELECT * FROM `tblequipments`");

		while($r = $admins->fetch()){
			echo "<tr>";
			echo "<td>".$r['eqid']."</td>";
			echo "<td>".$r['name']."</td>";
			echo "<td>".$r['count']."</td>";
			echo '<td><a class="btn btn-sm btn-info" onclick="updateAdmin('.$r['eqid'].')"> <span class="glyphicon glyphicon-pencil"></span> Edit</a> | <a class="btn btn-sm btn-danger" onclick="deleteAdmin('.$r['eqid'].')"><span class=
			"glyphicon glyphicon-trash"></span> Delete</a></td>';
			echo "</tr>";
		}
	}

	function deleteAdmin()
	{
		include '../config/config.php';
		$id = $_POST['id'];
    // prepare sql and bind parameters
		$stmt = $conn->prepare("DELETE FROM `tblequipments` WHERE `eqid`=:id");
		$stmt->bindParam(':id', $id);
		$stmt->execute();


		echo "<tr><script type='text/javascript'>
		$(document).ready(function(){
			$('#msgtitle').text('Success');
			$('#modalmsg').html('Equipment successfully deleted!');
			$('#msgmodalbtn').text('Close');
			$('#msgmodalbtn').attr('class', 'btn btn-success pull-right');
			$('#msgmodalheader').attr('class', 'modal-header modal-header-success');
			$('#msgmodal').modal('show');
		});
		</script></tr>";


		showAdmins();
	}

	function showUpdateAdmin()
	{
		include '../config/config.php';
		$id = $_POST['id'];

		$stmt = $conn->prepare("SELECT * FROM `tblequipments` WHERE `eqid`=:id");
		$stmt->bindParam(':id',$id);
		$stmt->execute(); 
		$row = $stmt->fetch();

		$eqid = secure($row['eqid']);
		$eqname = secure($row['name']);
		$count = secure($row['count']);


		echo json_encode(array(
			"eqid" => $eqid, 
			"eqname" => $eqname, 
			"count" => $count
		));

	}	


	function updateEquip()
	{
		include '../config/mconfig.php';


		$errors = array();

		$u_id = secure($_POST['u_id']);
		$u_eqname = secure($_POST['u_eqname']);
		$u_quantity = secure($_POST['u_quantity']);




	/*

	ALERT ALERT ALERT ==== NO VALIDATION
	if(strlen($_POST['u_fname']) == 0){
		array_push($errors, "First Name can not be blank!");
	}else{
		if(ContainsNumbers($_POST['u_fname'])){
			array_push($errors, "First Name contains number!");
		}
	}


	*/



	if(count($errors) > 0 )
	{

		echo "<tr><script type='text/javascript'>
		$(document).ready(function(){
			$('#msgtitle').text('Error');
			$('#modalmsg').html(\"".implode("<br />",$errors)."\");
			$('#msgmodalbtn').text('Close');
			$('#msgmodalbtn').attr('class', 'btn btn-danger pull-right');
			$('#msgmodalheader').attr('class', 'modal-header modal-header-danger');
			$('#msgmodal').modal('show');
		});</script></tr>";
	}
	else
	{

	// prepare and bind
		$stmt = $conn->prepare("UPDATE `tblequipments` SET `name`=?,`count`=? WHERE eqid=?");
		$stmt->bind_param("sii", $u_eqname, $u_quantity, $u_id);

		$stmt->execute();



	}



	showAdmins();


}

?>