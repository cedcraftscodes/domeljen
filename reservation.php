<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>DOMELJEN Events Unlimited Inc</title>
  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link href="css/animate.min.css" rel="stylesheet">
  <link href="css/prettyPhoto.css" rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">
  <link href="css/responsive.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="calendar/style.css">
  <script src="js/jquery.js"></script>
    <script src="calendar/jquery.min.js"></script>
  <style type="text/css">
  .ScrollStyle
  {
    max-height: 550px;
    overflow-y: scroll;
  }

  .ScrollStyle2
  {
    max-height: 290px;
    overflow-y: scroll;
  }
</style>
</head>
<body class="homepage">
  <?php include 'navbar.php'; ?>

  <section id="portfolio">
    <form id="reserve-form">
     <div class="container">
      <div class="center">
       <h2>Reservation</h2>
       <p class="lead" id="test"> Fill up the required fields.</p>

     </div>

     <div class="col-md-7">

       <h2>Customer Information</h2>
       <div class="col-lg-12 well">
        <div class="row">

          <div class="col-sm-12">
           <div class="row">
            <div class="col-sm-6 form-group">
             <label>First Name</label>
             <input type="text" placeholder="Enter First Name Here.." class="form-control" name="firstname" pattern="[a-zA-Z\s]{1,}" title="Letters only!" required="">
           </div>
           <div class="col-sm-6 form-group">
             <label>Last Name</label>
             <input type="text" placeholder="Enter Last Name Here.." class="form-control" name="lastname" pattern="[a-zA-Z\s]{1,}" title="Letters only!" required="">
           </div>
         </div>
         <div class="form-group">
          <label>Address</label>
          <textarea placeholder="Enter Address Here.." rows="3" class="form-control" name="address"></textarea>
        </div>
        <div class="row">
          <div class="col-sm-4 form-group">
           <label>City</label>
           <input type="text" placeholder="Enter City Name Here.." class="form-control" name="city" pattern="[a-zA-Z\s]{1,}" title="Letters only!" required="">
         </div>
         <div class="col-sm-4 form-group">
           <label>State</label>
           <input type="text" placeholder="Enter State Name Here.." class="form-control" name="state" pattern="[a-zA-Z\s]{1,}" title="Letters only!" required="">
         </div>
         <div class="col-sm-4 form-group">
           <label>Zip</label>
           <input type="text" placeholder="Enter Zip Code Here.." class="form-control" name="zip"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' >
         </div>
       </div>
       <div class="row">
        <div class="col-sm-6 form-group">
         <label>Title</label>
         <input type="text" placeholder="Enter Designation Here.." class="form-control" name="title">
       </div>
       <div class="col-sm-6 form-group">
         <label>Company</label>
         <input type="text" placeholder="Enter Company Name Here.." class="form-control" name="company">
       </div>
     </div>
     <div class="form-group">
      <label>Phone Number</label>
      <input type="text" placeholder="Enter Phone Number Here.." class="form-control" name="phone" required="">
    </div>
    <div class="form-group">
      <label>Email Address</label>
      <input type="email" placeholder="Enter Email Address Here.." class="form-control" name="email" required="">
      <input type="hidden" name="action" value="reserveNow">
    </div>

  </div>

</div>
</div>
<h2>Add Ons</h2>
<div class="col-lg-12 well">
  <div class="row">
   <div class="form-group">
    <label>Other Services</label>
    <div class="form-group ScrollStyle">

      <?php 
      include 'admin/config/config.php';
      $admins = $conn->query("SELECT * FROM `tbladdons` ORDER BY `addonname`");
      while($r = $admins->fetch()){
        echo "<p class='pull-right'>PHP ".$r['price']."</p>
        <div class='checkbox'>
        <label>
        <input class='addon' onchange='computeTotal()' type='checkbox' name='addons[]'  oncheckchange='computeTotal()' value='".$r['addonid']."'>".$r['addonname']."
        </label>
        </div>";
        echo "<ul>";

        $includes = $conn->query("SELECT `addeqid`, eq.name, `quantity` FROM `tbladdons_equip` as ae
          INNER JOIN tblequipments as eq 
          on eq.eqid = ae.`eqid`
          WHERE `addonid`=".$r['addonid']);

        while($rs = $includes->fetch()){
          echo "<li>".$rs['quantity']." ".$rs['name']."</li>";
        }

        echo "</ul>";
      }
      ?>
    </div>
  </div>
</div>


</div>

</div>
<div class="col-md-5">
 <h2>Event, Package and Others</h2>
 <div class="col-lg-12 well">
  <div class="row">

    <div class="col-sm-12">
     <div class="form-group">
      <label>Event Date</label>
      <input id="date" type="date" name="eventdate" class="form-control" required="">
      <label>Event Type</label>
      <select class="form-control" name="eventtype" required="">
       <option value="">Select Event Type</option>
       <option>Concerts</option>
       <option>Company Events</option>
       <option>School Events</option>
       <option>Religious Events</option>
       <option>Campus Events</option>
       <option>Fashion Shows</option>
       <option>Weddings</option>
       <option>Birthday/Debut</option>
     </select>
   </div>
   <div class="form-group">
    <label>PROFESSIONAL AUDIO EQUIPMENTS Package</label>
    <select class="form-control" id="cboaudiopack" name="cboaudiopack" required="">
     <option value="">Select AUDIO Package</option>
     <option value="0">I do not need audio equipments!</option>
     <?php 
     include 'admin/config/config.php';
     $admins = $conn->query("SELECT * FROM `tblpackages` WHERE `packagetype`='sound'");
     while($r = $admins->fetch()){
      echo "<option value=".$r['packid'].">".$r['packname']."</option>";
    }
    ?>
  </select>
</div>
<label>Package Info</label>
<div id="packinfo_sounds" class="ScrollStyle2">
</div>
<br/>
<div class="form-group">
  <label>PROFESSIONAL LIGHTS SYSTEM Package</label>
  <select class="form-control" id="cbolightspack" name="cbolightspack" required="">
   <option value="">Select LIGHTS Package</option>
   <option value="0">I do not need lights equipments!</option>
   <?php 
   include 'admin/config/config.php';
   $admins = $conn->query("SELECT * FROM `tblpackages` WHERE `packagetype`='lights'");
   while($r = $admins->fetch()){
    echo "<option value=".$r['packid'].">".$r['packname']."</option>";
  }
  ?>
</select>
</div>
<label>Package Info</label>
<div id="packinfo_lights" class="ScrollStyle2">
</div>
<br/>
<div class="form-group">
  <label>Coupon Code</label>
  <input type="text" name="coupon" id="promocode" onkeyup="computeTotal()" class="form-control">
</div>
<h2 >Grand Total</h2>
<h1 class="pull-right" style="color: black;" id="gtotal"></h1>
<br/><br/><br/>
<input type="submit" class="btn btn-lg btn-success" value="Reserve Now"></input>                   
</div>

</div>

</div>
</div>

</div>
</div>
</form>
</section>

<div class="center">
  <h2>Events</h2>
 <p class="lead" id="test"> Display all reservations.</p>

</div>

<div class="row">
  <?php include_once('calendar/functions.php'); ?>

  <div id="calendar_div">
    <?php echo getCalender(); ?>
  </div>

</div>
<!--/#portfolio-item-->
<?php include 'footer.php'; ?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/jquery.isotope.min.js"></script>   
<script src="js/wow.min.js"></script>
<script src="js/main.js"></script>
<!-- jQuery -->
<script src="vendors/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">

  Number.prototype.formatMoney = function(c, d, t){
    var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
    j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
  };


  $(document).ready(function(){



    //Add Form submit handler
    $('#reserve-form').submit(function(e){


      e.preventDefault();


      var formData = new FormData(this);

      $.ajax({

        url: 'reservationfunction.php',
        type: 'POST',
        cache: false,
        data: formData,
        async: false,
        processData: false,
        contentType: false,
        dataType: 'html',
        success: function(result)
        {
          if(result.trim() == "success"){
            window.location = "success.php";
          }
        },
        error: function(a,b,c)
        { alert(a.responseText);
          $('#msgtitle').text('Something Went Wrong!');
          $('#modalmsg').text('Please contant administrator for assistance!');
          $('#msgmodalbtn').text('Close');
          $('#msgmodalbtn').attr('class', 'btn btn-danger pull-right');
          $('#msgmodalheader').attr('class', 'modal-header modal-header-danger');
          $('#msgmodal').modal('show');
        }
      })
    });

    $( "#cbolightspack" ).change(function() {
     var packid = $('#cbolightspack').val();

     $.ajax({
       url : 'reservationfunction.php',
       type : 'POST',
       data : {packid : packid, action : 'showLightInfo'},
       dataType: 'html',
       success : function(result)
       {
         $('#packinfo_lights').html(result);
       },
       error: function()
       {
         $('#msgtitle').text('Something Went Wrong!');
         $('#modalmsg').text('Please contant administrator for assistance!');
         $('#msgmodalbtn').text('Close');
         $('#msgmodalbtn').attr('class', 'btn btn-danger pull-right');
         $('#msgmodalheader').attr('class', 'modal-header modal-header-danger');
         $('#msgmodal').modal('show');
       }
     });

     computeTotal();
   });


    $( "#cboaudiopack" ).change(function() {
     var packid = $('#cboaudiopack').val();

     $.ajax({
       url : 'reservationfunction.php',
       type : 'POST',
       data : {packid : packid, action : 'showLightInfo'},
       dataType: 'html',
       success : function(result)
       {
         $('#packinfo_sounds').html(result);
       },
       error: function(a,b,c)
       {

         $('#msgtitle').text('Something Went Wrong!');
         $('#modalmsg').text('Please contant administrator for assistance!');
         $('#msgmodalbtn').text('Close');
         $('#msgmodalbtn').attr('class', 'btn btn-danger pull-right');
         $('#msgmodalheader').attr('class', 'modal-header modal-header-danger');
         $('#msgmodal').modal('show');
       }

     });
     computeTotal();
   });
  });

  function computeTotal(){
    var spackid = $('#cbolightspack').val();
    var apackid = $('#cboaudiopack').val();
    var pcode = $('#promocode').val();

    var selectedAddons = new Array();
    $('input[name="addons[]"]:checked').each(function() {
      selectedAddons.push(this.value);


    });


    $.ajax({
     url : 'reservationfunction.php',
     type : 'POST',
     data : {spackid : spackid, apackid:apackid, addons: selectedAddons, promocode: pcode ,action : 'computeTotal'},
     dataType: 'json',
     success : function(result)
     {
       $('#gtotal').text("PHP " + result.total.formatMoney(2, '.', ','));

     },
     error: function(a,b,c)
     {
       $('#msgtitle').text('Something Went Wrong!');
       $('#modalmsg').text('Please contant administrator for assistance!');
       $('#msgmodalbtn').text('Close');
       $('#msgmodalbtn').attr('class', 'btn btn-danger pull-right');
       $('#msgmodalheader').attr('class', 'modal-header modal-header-danger');
       $('#msgmodal').modal('show');
     }
   });
  }


</script>
</body>
</html>
