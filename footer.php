
<section id="bottom">
    <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3>Company</h3>
                    <ul>
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">Terms and Condition</a></li>
                        <li><a href="#">Gallery</a></li>                           
                    </ul>
                </div>    
            </div><!--/.col-md-3-->

            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3>Support</h3>
                    <ul>
                        <li><a href="#">Faq</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Disclaimer</a></li>
                        <li><a href="#">Documentation</a></li>                          
                    </ul>
                </div>    
            </div><!--/.col-md-3-->

            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3>Services</h3>
                    <ul>
                        <li><a href="#">Lights and Sounds</a></li>
                        <li><a href="#">Band Performers</a></li>
                        <li><a href="#">Stage</a></li>
                        <li><a href="#">Hosts</a></li>
                    </ul>
                </div>    
            </div><!--/.col-md-3-->

            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3>Our Partners</h3>
                    <ul>
                        <li><a href="#">Rappy</a></li>
                        <li><a href="#">Nestl</a></li>
                        <li><a href="#">Vinaa</a></li>
                        <li><a href="#">Icorrelate</a></li>                           
                    </ul>
                </div>    
            </div><!--/.col-md-3-->
        </div>
    </div>
</section><!--/#bottom-->

<div class="top-bar">
  <div class="container">
   <div class="row">
       <div class="col-lg-12">
           <div class="social">
              <ul class="social-share">
               <li><a href="#"><i class="fa fa-facebook"></i></a></li>
               <li><a href="#"><i class="fa fa-twitter"></i></a></li>
               <li><a href="#"><i class="fa fa-linkedin"></i></a></li> 
               <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
               <li><a href="#"><i class="fa fa-skype"></i></a></li>
           </ul>
       </div>
   </div>
</div>
</div><!--/.container-->
</div><!--/.top-bar-->



<footer id="footer" class="midnight-blue">
 <div class="container">
    <div class="row">
       <div class="col-sm-6">
          &copy; 2018 <a target="_blank">DOMELJEN Events Unlimited Inc. </a>. All Rights Reserved.
      </div>
                <!-- 
                    All links in the footer should remain intact. 
                    Licenseing information is available at: http://bootstraptaste.com/license/
                    You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Gp
                -->
                <div class="col-sm-6">
                	<ul class="pull-right">
                		<li><a href="index.php">Home</a></li>
                		<li><a href="about-us.php">About Us</a></li>
                		<li><a href="gallery.php">Gallery</a></li>
                		<li><a href="contact-us.php">Contact Us</a></li>
                	</ul>
                </div>
            </div>
        </div>
</footer><!--/#footer-->