<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>DOMELJEN Events Unlimited Inc.</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/animate.min.css" rel="stylesheet">
	<link href="css/prettyPhoto.css" rel="stylesheet">      
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->       

</head>
<body class="homepage">   
	<?php include 'navbar.php'; ?>
	<section id="about-us">
		<div class="container">			
			<div class="skill-wrap clearfix">			
				<div class="center wow fadeInDown">
					<h2><span>DOMELJEN Events Unlimited Inc</span></h2>
					<p class="lead">

						With the extreme passion of founder to music and sounds, to parties and gatherings, from creative visualization, décor and entertainment, the creation and formulation of the business established early 2002. The formation of the company composed of young professionals, combined with their field of expertise, skills and experiences.
						Domeljen Events Unlimited had its bright start in July 2009. Domeljen Events Unlimited is a multi-business in the field of social events and different festivities and functions, with a mindset of QUALITY SERVICE above anything else. Using advance and top of the line technical equipment, with trusted and trained personnel, and offers affordable rates, the company progressively achieves its hallmark as Event Specialist.
					</p>
				</div>

				<div class="center wow fadeInDown">
					<h2>Our <span>MISSION</span></h2>
					<p class="lead">To make memorable events more meaningful, constantly strive to meet and exceed customers’ needs and expectations, as taking pride in making the best event possible, WHAT YOU WISH IS WHAT WE GIVE</p>
				</div>

				<div class="center wow fadeInDown">
					<h2>Our <span>VISION</span></h2>
					<p class="lead">To provide the best in events organization, to provide quality service combine with excellent performance while establishing successful relationship with customer and clients, WHERE EXPERIENCE IS REMARKABLE </p>
				</div>


			</div>


		</section><!--/about-us-->


		<?php include 'footer.php'; ?>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.prettyPhoto.js"></script>
		<script src="js/jquery.isotope.min.js"></script>   
		<script src="js/wow.min.js"></script>
		<script src="js/main.js"></script>
	</body>
	</html>