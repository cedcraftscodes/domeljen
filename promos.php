<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DOMELJEN Events Unlimited Inc</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">      
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/cards.css">
	 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->       

</head>
<body class="homepage">   
    <?php include 'navbar.php'; ?>

    <section id="portfolio">
        <div class="container">
            <div class="center">
             <h2>Promos</h2>
             <p class="lead">Get discount by applying the promo codes below! </p>
         </div>

         <div class="row">


            <?php 

            include 'admin/config/config.php';

            $admins = $conn->query("SELECT * FROM `tblpromos`");

            while($r = $admins->fetch()){

                ?>

                <div class="col-md-4" >
                    <div class="card-2">
                        <div class="card-block">
                            <div class="container-fluid" style="padding: 30px">
                                <div class="row">
                                    <div class="col-md-12" >
                                        <h3 style="color: black;">SAVE <span style="color: red;"><?php echo $r['discount']; ?></span> PESOS for minimum reservation price of: <?php echo $r['minpurchase']; ?></h3>
                                    </div>
                                </div>
                                <p> <h2><code><?php echo $r['promocode']; ?></code></h2></p>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
            }
            ?>

        </div>
    </div>
</section><!--/#portfolio-item-->
<?php include 'footer.php'; ?>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/jquery.isotope.min.js"></script>   
<script src="js/wow.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>