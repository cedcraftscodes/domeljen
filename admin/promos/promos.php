<?php 


if(session_id()){}else{session_start();}

if(isset($_POST['action']) && !empty($_POST['action']))
{

	$action = $_POST['action'];
	switch ($action) {
		case 'addEquipment':
		addEquipment();
		break;
		case 'showAdmins':
		showAdmins();
		break;
		case 'deleteAdmin':
		deleteAdmin();
		break;
		case 'showUpdateAdmin':
		showUpdateAdmin();
		break;
		case 'updateEquip':
		updateEquip();
		break;
		default:
				# code...
		break;
	}
}


function secure($str){
	return strip_tags(trim(htmlspecialchars($str)));
}


function ContainsNumbers($String){
	return preg_match('/\\d/', $String) > 0;
}

function addEquipment()
{
	include '../config/mconfig.php';

	/*
		Validations 
	*/
		$errors = array();

		/*

		if(strlen($_POST['fname']) == 0){
			array_push($errors, "First Name can not be blank!");
		}else{
			if(ContainsNumbers($_POST['fname'])){
				array_push($errors, "First Name contains number!");
			}
		}
		*/

		
		if(count($errors) > 0 )
		{

			echo "<tr><script type='text/javascript'>
			$(document).ready(function(){
				$('#msgtitle').text('Error');
				$('#modalmsg').html(\"".implode("<br />",$errors)."\");
				$('#msgmodalbtn').text('Close');
				$('#msgmodalbtn').attr('class', 'btn btn-danger pull-right');
				$('#msgmodalheader').attr('class', 'modal-header modal-header-danger');
				$('#msgmodal').modal('show');



			});
		</script></tr>";
	}
	else
	{

		$eqname = secure($_POST['eqname']);
		$quantity = secure($_POST['quantity']);
		$minpurchase = secure($_POST['minpurchase']);
		
		// prepare and bind
		$stmt = $conn->prepare("INSERT INTO `tblpromos`( `promocode`, `discount`, `minpurchase`) VALUES (?, ?, ?)");
		$stmt->bind_param("sss", $eqname, $quantity, $minpurchase);
		$stmt->execute();

		echo "<tr><script type='text/javascript'>
		$(document).ready(function(){
			$('#msgtitle').text('Success');
			$('#modalmsg').html('Promo successfully added!');
			$('#msgmodalbtn').text('Close');
			$('#msgmodalbtn').attr('class', 'btn btn-success pull-right');
			$('#msgmodalheader').attr('class', 'modal-header modal-header-success');
			$('#msgmodal').modal('show');
		});
	</script></tr>";
}
showAdmins();


}


function showAdmins()
{
	include '../config/config.php';

	$admins = $conn->query("SELECT * FROM `tblpromos`");

	while($r = $admins->fetch()){
		echo "<tr>";
		echo "<td>".$r['promoid']."</td>";
		echo "<td>".$r['promocode']."</td>";
		echo "<td>".$r['discount']."</td>";
		echo "<td>".$r['minpurchase']."</td>";

		echo '<td><a class="btn btn-sm btn-info" onclick="updateAdmin('.$r['promoid'].')"> <span class="glyphicon glyphicon-pencil"></span> Edit</a> | <a class="btn btn-sm btn-danger" onclick="deleteAdmin('.$r['promoid'].')"><span class=
		"glyphicon glyphicon-trash"></span> Delete</a></td>';
		echo "</tr>";
	}
}


function deleteAdmin()
{
	include '../config/config.php';
	$id = $_POST['id'];
    // prepare sql and bind parameters
	$stmt = $conn->prepare("DELETE FROM `tblpromos` WHERE `promoid`=:id");
	$stmt->bindParam(':id', $id);
	$stmt->execute();


	echo "<tr><script type='text/javascript'>
	$(document).ready(function(){
		$('#msgtitle').text('Success');
		$('#modalmsg').html('Promo successfully deleted!');
		$('#msgmodalbtn').text('Close');
		$('#msgmodalbtn').attr('class', 'btn btn-success pull-right');
		$('#msgmodalheader').attr('class', 'modal-header modal-header-success');
		$('#msgmodal').modal('show');
	});
	</script></tr>";


showAdmins();
}

function showUpdateAdmin()
{
	include '../config/config.php';
	$id = $_POST['id'];

	$stmt = $conn->prepare("SELECT * FROM `tblpromos` WHERE `promoid`=:id");
	$stmt->bindParam(':id',$id);
	$stmt->execute(); 
	$row = $stmt->fetch();

	$eqid = secure($row['promoid']);
	$eqname = secure($row['promocode']);
	$count = secure($row['discount']);
	$minpurchase = secure($row['minpurchase']);

	echo json_encode(array(
		"eqid" => $eqid, 
		"eqname" => $eqname, 
		"count" => $count,
		"minpurchase" => $minpurchase
	));

}	


function updateEquip()
{
	include '../config/mconfig.php';


	$errors = array();

	$u_id = secure($_POST['u_id']);
	$u_eqname = secure($_POST['u_eqname']);
	$u_quantity = secure($_POST['u_quantity']);
	$minpurchase = secure($_POST['u_minpurchase']);



	/*

	ALERT ALERT ALERT ==== NO VALIDATION
	if(strlen($_POST['u_fname']) == 0){
		array_push($errors, "First Name can not be blank!");
	}else{
		if(ContainsNumbers($_POST['u_fname'])){
			array_push($errors, "First Name contains number!");
		}
	}


	*/



	if(count($errors) > 0 )
	{

		echo "<tr><script type='text/javascript'>
		$(document).ready(function(){
			$('#msgtitle').text('Error');
			$('#modalmsg').html(\"".implode("<br />",$errors)."\");
			$('#msgmodalbtn').text('Close');
			$('#msgmodalbtn').attr('class', 'btn btn-danger pull-right');
			$('#msgmodalheader').attr('class', 'modal-header modal-header-danger');
			$('#msgmodal').modal('show');
		});</script></tr>";
	}
	else
	{

	// prepare and bind
	$stmt = $conn->prepare("UPDATE `tblpromos` SET `promocode`=?,`discount`=?, minpurchase=? WHERE promoid=?");
	$stmt->bind_param("sddi", $u_eqname, $u_quantity, $minpurchase, $u_id);

	$stmt->execute();



	}

	echo "<tr><script type='text/javascript'>
	$(document).ready(function(){
		$('#msgtitle').text('Success');
		$('#modalmsg').html('Promo successfully updated!');
		$('#msgmodalbtn').text('Close');
		$('#msgmodalbtn').attr('class', 'btn btn-success pull-right');
		$('#msgmodalheader').attr('class', 'modal-header modal-header-success');
		$('#msgmodal').modal('show');
	});
	</script></tr>";


	showAdmins();


}

?>