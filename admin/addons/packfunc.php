<?php 


if(session_id()){}else{session_start();}

if(isset($_POST['action']) && !empty($_POST['action']))
{

	$action = $_POST['action'];
	switch ($action) {
		case 'addPackage':
		addPackage();
		break;

		case 'showPackages':
		showPackages();
		break;

		case 'deletePack':
		deletePack();
		break;

		case 'updatePackage':
		updatePackage();
		break;
		default:
				# code...
		break;
	}
}





function deletePack()
{
	include '../config/config.php';
	$id = $_POST['id'];
    // prepare sql and bind parameters
	$stmt = $conn->prepare("DELETE FROM `tbladdons` WHERE `addonid`=:id");
	$stmt->bindParam(':id', $id);
	$stmt->execute();

	$stmt = $conn->prepare("DELETE FROM `tbladdons_equip` WHERE `addonid`=:id");
	$stmt->bindParam(':id', $id);
	$stmt->execute();

	echo "<tr><script type='text/javascript'>
	$(document).ready(function(){
		$('#msgtitle').text('Success');
		$('#modalmsg').html('Addon successfully deleted!');
		$('#msgmodalbtn').text('Close');
		$('#msgmodalbtn').attr('class', 'btn btn-success pull-right');
		$('#msgmodalheader').attr('class', 'modal-header modal-header-success');
		$('#msgmodal').modal('show');
	});
	</script></tr>";


	showPackages();
}


function showPackages()
{
	include '../config/config.php';

	$admins = $conn->query("SELECT `addonid`, `addonname`, `Price` FROM `tbladdons`");

	while($r = $admins->fetch()){
		echo "<tr>";
		echo "<td>".$r['addonid']."</td>";
		echo "<td>".$r['addonname']."</td>";
		echo "<td>".$r['Price']."</td>";
		echo '<td><a class="btn btn-sm btn-info" href="editservice.php?id='.$r['addonid'].'"> <span class="glyphicon glyphicon-pencil"></span> Edit</a> | <a class="btn btn-sm btn-danger" onclick="deleteAdmin('.$r['addonid'].')"><span class=
		"glyphicon glyphicon-trash"></span> Delete</a></td>';
		echo "</tr>";
	}
}

function updatePackage(){
	//delete 
	
	$quantities = $_POST['quantities'];
	$packname =  secure($_POST['packname']);
	$price =  secure($_POST['price']);
	$packid =  secure($_POST['packid']);


	$totalqty = 0;

	foreach ($quantities as $id => $qty) 
	{
		$totalqty+=(int)$qty;
	}



	include '../config/config.php';
	$stmt = $conn->prepare("DELETE FROM tbladdons_equip WHERE `addonid`=:id");
	$stmt->bindParam(':id', $packid);
	$stmt->execute(); 

	$time = time();

	$stmt = $conn->prepare("UPDATE `tbladdons` SET `addonname`=:name, `Price`=:price WHERE `addonid`=:id");
	$stmt->bindParam(':name',$packname);
	$stmt->bindParam(':price',$price);
	$stmt->bindParam(':id',$packid);
	$stmt->execute(); 

	foreach ($quantities as $id => $qty) {
		$pid = $id;
		$quantity = (int)$qty;

		if($quantity == 0)
			continue;

		$stmt = $conn->prepare("INSERT INTO `tbladdons_equip`(`eqid`, `addonid`, `quantity`) VALUES (:eq, :pid, :qty)");
		$stmt->bindParam(':eq',$id);
		$stmt->bindParam(':pid',$packid);
		$stmt->bindParam(':qty',$quantity);
		$stmt->execute(); 

	}

	echo json_encode(array(
		"message" => 'Updating addon is successful!', 
		"success" => true
	));


}





function addPackage(){
	$quantities = $_POST['quantities'];
	$packname =  secure($_POST['packname']);
	$price =  secure($_POST['price']);

	$totalqty = 0;

	foreach ($quantities as $id => $qty) 
	{
		$totalqty+=(int)$qty;
	}

	$time = time();

	include '../config/config.php';
	$stmt = $conn->prepare("INSERT INTO `tbladdons`(`addonname`, `price`) VALUES (:name, :price)");

	$stmt->bindParam(':name',$packname);
	$stmt->bindParam(':price',$price);
	$stmt->execute(); 
	$delid = $conn->lastInsertId();

	foreach ($quantities as $id => $qty) {
		$pid = $id;
		$quantity = (int)$qty;

		if($quantity == 0)
			continue;

		$stmt = $conn->prepare("INSERT INTO `tbladdons_equip`(`eqid`, `addonid`, `quantity`) VALUES (:eq, :pid, :qty)");
		$stmt->bindParam(':eq',$id);
		$stmt->bindParam(':pid',$delid);
		$stmt->bindParam(':qty',$quantity);
		$stmt->execute(); 

	}

	echo json_encode(array(
		"message" => 'Adding addon is successful!', 
		"success" => true
	));


}




function secure($str){
	return strip_tags(trim(htmlspecialchars($str)));
}


function ContainsNumbers($String){
	return preg_match('/\\d/', $String) > 0;
}


?>