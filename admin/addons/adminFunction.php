<?php 


if(session_id()){}else{session_start();}

if(isset($_POST['action']) && !empty($_POST['action']))
{

	$action = $_POST['action'];
	switch ($action) {
		case 'addEquipment':
		addEquipment();
		break;
		case 'showAdmins':
		showAdmins();
		break;
		case 'deleteAdmin':
		deleteAdmin();
		break;
		case 'showUpdateAdmin':
		showUpdateAdmin();
		break;
		case 'updateEquip':
		updateEquip();
		break;
		default:
				# code...
		break;
	}
}


function secure($str){
	return strip_tags(trim(htmlspecialchars($str)));
}


function ContainsNumbers($String){
	return preg_match('/\\d/', $String) > 0;
}

function addEquipment()
{
	include '../config/mconfig.php';

	/*
		Validations 
	*/
		$errors = array();

		/*

		if(strlen($_POST['fname']) == 0){
			array_push($errors, "First Name can not be blank!");
		}else{
			if(ContainsNumbers($_POST['fname'])){
				array_push($errors, "First Name contains number!");
			}
		}
		*/

		
		if(count($errors) > 0 )
		{

			echo "<tr><script type='text/javascript'>
			$(document).ready(function(){
				$('#msgtitle').text('Error');
				$('#modalmsg').html(\"".implode("<br />",$errors)."\");
				$('#msgmodalbtn').text('Close');
				$('#msgmodalbtn').attr('class', 'btn btn-danger pull-right');
				$('#msgmodalheader').attr('class', 'modal-header modal-header-danger');
				$('#msgmodal').modal('show');



			});
		</script></tr>";
	}
	else
	{


		$eqname = secure($_POST['eqname']);
		$quantity = secure($_POST['quantity']);

		// prepare and bind
		$stmt = $conn->prepare("INSERT INTO `tblequipments`( `name`, `count`)  VALUES (?, ?)");
		$stmt->bind_param("ss", $eqname, $quantity);
		$stmt->execute();



		echo "<tr><script type='text/javascript'>
		$(document).ready(function(){
			$('#msgtitle').text('Success');
			$('#modalmsg').html('Equipment successfully added!');
			$('#msgmodalbtn').text('Close');
			$('#msgmodalbtn').attr('class', 'btn btn-success pull-right');
			$('#msgmodalheader').attr('class', 'modal-header modal-header-success');
			$('#msgmodal').modal('show');
		});
	</script></tr>";
}
showAdmins();


}




function showAdmins()
{
	include '../config/config.php';

	$admins = $conn->query("SELECT * FROM `tblequipments`");

	while($r = $admins->fetch()){
		echo "<tr>";
		echo "<td>".$r['eqid']."</td>";
		echo "<td>".$r['name']."</td>";
		echo "<td>".$r['count']."</td>";
		echo '<td><a class="btn btn-sm btn-info" onclick="updateAdmin('.$r['eqid'].')"> <span class="glyphicon glyphicon-pencil"></span> Edit</a> | <a class="btn btn-sm btn-danger" onclick="deleteAdmin('.$r['eqid'].')"><span class=
		"glyphicon glyphicon-trash"></span> Delete</a></td>';
		echo "</tr>";
	}
}

function deleteAdmin()
{
	include '../config/config.php';
	$id = $_POST['id'];
    // prepare sql and bind parameters
	$stmt = $conn->prepare("DELETE FROM `tblequipments` WHERE `eqid`=:id");
	$stmt->bindParam(':id', $id);
	$stmt->execute();


	echo "<tr><script type='text/javascript'>
	$(document).ready(function(){
		$('#msgtitle').text('Success');
		$('#modalmsg').html('Equipment successfully deleted!');
		$('#msgmodalbtn').text('Close');
		$('#msgmodalbtn').attr('class', 'btn btn-success pull-right');
		$('#msgmodalheader').attr('class', 'modal-header modal-header-success');
		$('#msgmodal').modal('show');
	});
	</script></tr>";


showAdmins();
}

function showUpdateAdmin()
{
	include '../config/config.php';
	$id = $_POST['id'];

	$stmt = $conn->prepare("SELECT * FROM `tblequipments` WHERE `eqid`=:id");
	$stmt->bindParam(':id',$id);
	$stmt->execute(); 
	$row = $stmt->fetch();

	$eqid = secure($row['eqid']);
	$eqname = secure($row['name']);
	$count = secure($row['count']);


	echo json_encode(array(
		"eqid" => $eqid, 
		"eqname" => $eqname, 
		"count" => $count
	));

}	


function updateEquip()
{
	include '../config/mconfig.php';


	$errors = array();

	$u_id = secure($_POST['u_id']);
	$u_eqname = secure($_POST['u_eqname']);
	$u_quantity = secure($_POST['u_quantity']);




	/*

	ALERT ALERT ALERT ==== NO VALIDATION
	if(strlen($_POST['u_fname']) == 0){
		array_push($errors, "First Name can not be blank!");
	}else{
		if(ContainsNumbers($_POST['u_fname'])){
			array_push($errors, "First Name contains number!");
		}
	}


	*/



	if(count($errors) > 0 )
	{

		echo "<tr><script type='text/javascript'>
		$(document).ready(function(){
			$('#msgtitle').text('Error');
			$('#modalmsg').html(\"".implode("<br />",$errors)."\");
			$('#msgmodalbtn').text('Close');
			$('#msgmodalbtn').attr('class', 'btn btn-danger pull-right');
			$('#msgmodalheader').attr('class', 'modal-header modal-header-danger');
			$('#msgmodal').modal('show');
		});</script></tr>";
	}
	else
	{

	// prepare and bind
	$stmt = $conn->prepare("UPDATE `tblequipments` SET `name`=?,`count`=? WHERE eqid=?");
	$stmt->bind_param("sii", $u_eqname, $u_quantity, $u_id);

	$stmt->execute();



	}



	showAdmins();


}

?>