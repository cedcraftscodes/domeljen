<?php
if(isset($_GET["query"]))
{

	$keyword = htmlspecialchars($_GET["query"], ENT_QUOTES, 'UTF-8');

	if(strlen($keyword) < 3){
		echo "<p> Minimum of 3 characters required!</p>";
	}else{
		include '../config/config.php';


		$searchResult = $conn->prepare("SELECT res.reservationid, CONCAT(cs.first_name, ' ', cs.last_name) as 'Name', cs.email, res.`eventtype`, res.`eventdate`, res.total, res.status, res.datesent
			FROM `tblreservation` as res
			INNER JOIN tblcustomer as cs 
			ON cs.custid = res.custid WHERE CONCAT(cs.first_name, ' ', cs.last_name) LIKE :kw ");
		$searchResult->bindValue(":kw", '%'.$keyword.'%') ;
		$searchResult->execute();
		$count=$searchResult->rowCount();

		if($count != 0){
			while($r = $searchResult->fetch()){
				echo "<tr>";
				echo "<td>".$r['reservationid']."</td>";
				echo "<td>".$r['Name']."</td>";
				echo "<td>".$r['email']."</td>";
				echo "<td>".$r['eventtype']."</td>";
				echo "<td>".$r['eventdate']."</td>";
				echo "<td>".$r['total']."</td>";
				echo "<td>".$r['status']."</td>";
				$dateadded = date("F j, Y, g:i a", $r["datesent"]);
				echo "<td>".$dateadded."</td>";

				echo '<td><a class="btn btn-sm btn-primary" href="showreservation.php?id="'.$r['reservationid'].'> <span class="glyphicon glyphicon-pencil"></span> Show</a><a class="btn btn-sm btn-info" onclick="updateAdmin('.$r['eqid'].')"><span class=
				"glyphicon glyphicon-trash"></span> Status</a></td>';
				echo "</tr>";
			}

		}else{
			echo "<p>No results found! </p>";
		}

	}

}


?>