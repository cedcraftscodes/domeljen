<?php 


if(session_id()){}else{session_start();}

if(isset($_POST['action']) && !empty($_POST['action']))
{

	$action = $_POST['action'];
	switch ($action) {
		case 'addEquipment':
		addEquipment();
		break;
		case 'showAdmins':
		showAdmins();
		break;
		case 'deleteAdmin':
		deleteAdmin();
		break;
		case 'showUpdateAdmin':
		showUpdateAdmin();
		break;
		case 'updateAdmin':
		updateAdmin();
		break;
		default:
				# code...
		break;
	}
}


function secure($str){
	return strip_tags(trim(htmlspecialchars($str)));
}

function ContainsNumbers($String){
	return preg_match('/\\d/', $String) > 0;
}

function addEquipment()
{
	include '../config/mconfig.php';

	/*
		Validations 
	*/
		$errors = array();

		/*

		if(strlen($_POST['fname']) == 0){
			array_push($errors, "First Name can not be blank!");
		}else{
			if(ContainsNumbers($_POST['fname'])){
				array_push($errors, "First Name contains number!");
			}
		}
		*/

		
		if(count($errors) > 0 )
		{

			echo "<tr><script type='text/javascript'>
			$(document).ready(function(){
				$('#msgtitle').text('Error');
				$('#modalmsg').html(\"".implode("<br />",$errors)."\");
				$('#msgmodalbtn').text('Close');
				$('#msgmodalbtn').attr('class', 'btn btn-danger pull-right');
				$('#msgmodalheader').attr('class', 'modal-header modal-header-danger');
				$('#msgmodal').modal('show');



			});
			</script></tr>";
		}
		else
		{


			$eqname = secure($_POST['eqname']);
			$quantity = secure($_POST['quantity']);

		// prepare and bind
			$stmt = $conn->prepare("INSERT INTO `tblequipments`( `name`, `count`)  VALUES (?, ?)");
			$stmt->bind_param("ss", $eqname, $quantity);
			$stmt->execute();

			echo "<tr><script type='text/javascript'>
			$(document).ready(function(){
				$('#msgtitle').text('Success');
				$('#modalmsg').html('Equipment successfully added!');
				$('#msgmodalbtn').text('Close');
				$('#msgmodalbtn').attr('class', 'btn btn-success pull-right');
				$('#msgmodalheader').attr('class', 'modal-header modal-header-success');
				$('#msgmodal').modal('show');
			});
			</script></tr>";
		}
		showAdmins();


	}




	function showAdmins()
	{
		include '../config/config.php';

		$admins = $conn->query("SELECT res.reservationid, CONCAT(cs.first_name, ' ', cs.last_name) as 'Name', cs.email, res.`eventtype`, res.`eventdate`, res.total, res.status, res.datesent
			FROM `tblreservation` as res
			INNER JOIN tblcustomer as cs 
			ON cs.custid = res.custid");

		while($r = $admins->fetch()){
			echo "<tr>";
			echo "<td>".$r['reservationid']."</td>";
			echo "<td>".$r['Name']."</td>";
			echo "<td>".$r['email']."</td>";
			echo "<td>".$r['eventtype']."</td>";
			echo "<td>".$r['eventdate']."</td>";
			echo "<td>".$r['total']."</td>";
			echo "<td>".$r['status']."</td>";
			$dateadded = date("F j, Y, g:i a", $r["datesent"]);
			echo "<td>".$dateadded."</td>";


			echo '<td><a class="btn btn-sm btn-primary" href="showreservation.php?id='.$r['reservationid'].'"> <span class="glyphicon glyphicon-pencil"></span> Show</a><a class="btn btn-sm btn-info" onclick="updateAdmin('.$r['reservationid'].')"><span class=
			"glyphicon glyphicon-trash"></span> Status</a></td>';
			echo "</tr>";
		}
	}

	function deleteAdmin()
	{
		include '../config/config.php';
		$id = $_POST['id'];
    // prepare sql and bind parameters
		$stmt = $conn->prepare("DELETE FROM `tblequipments` WHERE `eqid`=:id");
		$stmt->bindParam(':id', $id);
		$stmt->execute();


		echo "<tr><script type='text/javascript'>
		$(document).ready(function(){
			$('#msgtitle').text('Success');
			$('#modalmsg').html('Equipment successfully deleted!');
			$('#msgmodalbtn').text('Close');
			$('#msgmodalbtn').attr('class', 'btn btn-success pull-right');
			$('#msgmodalheader').attr('class', 'modal-header modal-header-success');
			$('#msgmodal').modal('show');
		});
		</script></tr>";


		showAdmins();
	}

	function showUpdateAdmin()
	{
		include '../config/config.php';
		$id = $_POST['id'];

		$stmt = $conn->prepare("SELECT * FROM `tblreservation` WHERE `reservationid`=:id");
		$stmt->bindParam(':id',$id);
		$stmt->execute(); 
		$row = $stmt->fetch();

		$status = secure($row['status']);
		$u_id = secure($row['reservationid']);

		echo json_encode(array(
			"status" => $status
		));

	}	


	function updateAdmin()
	{

		include '../config/config.php';

		$errors = array();
		$u_id = secure($_POST['u_id']);
		$status = secure($_POST['status']);

	/*

	ALERT ALERT ALERT ==== NO VALIDATION
	if(strlen($_POST['u_fname']) == 0){
		array_push($errors, "First Name can not be blank!");
	}else{
		if(ContainsNumbers($_POST['u_fname'])){
			array_push($errors, "First Name contains number!");
		}
	}


	*/



	if(count($errors) > 0 )
	{

		echo "<tr><script type='text/javascript'>
		$(document).ready(function(){
			$('#msgtitle').text('Error');
			$('#modalmsg').html(\"".implode("<br />",$errors)."\");
			$('#msgmodalbtn').text('Close');
			$('#msgmodalbtn').attr('class', 'btn btn-danger pull-right');
			$('#msgmodalheader').attr('class', 'modal-header modal-header-danger');
			$('#msgmodal').modal('show');
		});</script></tr>";
	}
	else
	{


		$stmt = $conn->prepare("SELECT * FROM `tblreservation` WHERE `reservationid`=:id");
		$stmt->bindParam(':id',$u_id);
		$stmt->execute(); 
		$row = $stmt->fetch();

		$dbstatus = secure($row['status']);

		if($status != $row['status'] ){

			if($status == "Done" || $status == "Cancelled"){


			//get all equipment on audio package
			//Increment all quantity
				$audioequip = $conn->query(
					"SELECT peq.eqid, peq.quantity FROM `tblreservation` as res 
					INNER JOIN tblpackages as pack 
					on pack.packid = res.soundpack
					INNER JOIN tblpackage_equip as peq
					ON peq.packid = pack.packid
					WHERE res.reservationid=".$u_id);

				while($r = $audioequip->fetch()){
					//Diminish; 
					$stmt = $conn->prepare("UPDATE `tblequipments` SET `count`=`count` + :ct WHERE `eqid`=:eqid");
					$stmt->bindParam(':ct', $r['quantity'], PDO::PARAM_INT);
					$stmt->bindParam(':eqid',$r['eqid']);
					$stmt->execute(); 
				}


			//get all equipment on audio package
			//Increment all quantity

				$lighteq = $conn->query(
					"SELECT peq.eqid, peq.quantity FROM `tblreservation` as res 
					INNER JOIN tblpackages as pack 
					on pack.packid = res.lightpack
					INNER JOIN tblpackage_equip as peq
					ON peq.packid = pack.packid
					WHERE res.reservationid=".$u_id);

				while($r = $lighteq->fetch()){
					$stmt = $conn->prepare("UPDATE `tblequipments` SET `count`=`count` + :ct WHERE `eqid`=:eqid");
					$stmt->bindParam(':ct', $r['quantity'], PDO::PARAM_INT);
					$stmt->bindParam(':eqid',$r['eqid']);
					$stmt->execute(); 
				}


			//get all addons equipement
			//increment that to database

				$addons = $conn->query(
					"SELECT ae.eqid, ae.quantity FROM `tblreservation_addons` as resa 
					INNER JOIN tbladdons as ado 
					ON ado.addonid = resa.addonid
					INNER JOIN tbladdons_equip as ae 
					on ae.addonid = ado.addonid
					WHERE resa.`reservationid`= ".$u_id);

				while($r = $addons->fetch()){
					$stmt = $conn->prepare("UPDATE `tblequipments` SET `count`=`count` + :ct WHERE `eqid`=:eqid");
					$stmt->bindParam(':ct', $r['quantity'], PDO::PARAM_INT);
					$stmt->bindParam(':eqid',$r['eqid']);
					$stmt->execute(); 
				}
				include '../config/mconfig.php';
			//update tblreseration , set status = $status
				$stmt = $conn->prepare("UPDATE `tblreservation` SET `status`=? WHERE reservationid=?");
				$stmt->bind_param("si", $status , $u_id);
				$stmt->execute();

			}else if($dbstatus == "Done" && $status == "Pending" || $status == "Pending Verification" || $status =="Ongoing"){

			//get all equipment on audio package
			//decrement all quantity
				$audioequip = $conn->query(
					"SELECT peq.eqid, peq.quantity FROM `tblreservation` as res 
					INNER JOIN tblpackages as pack 
					on pack.packid = res.soundpack
					INNER JOIN tblpackage_equip as peq
					ON peq.packid = pack.packid
					WHERE res.reservationid=".$u_id);

				while($r = $audioequip->fetch()){
					$stmt = $conn->prepare("UPDATE `tblequipments` SET `count`=`count` - :ct WHERE `eqid`=:eqid");
					$stmt->bindParam(':ct', $r['quantity'], PDO::PARAM_INT);
					$stmt->bindParam(':eqid',$r['eqid']);
					$stmt->execute(); 
				}


			//get all equipment on audio package
			//decrement all quantity

				$lighteq = $conn->query(
					"SELECT peq.eqid, peq.quantity FROM `tblreservation` as res 
					INNER JOIN tblpackages as pack 
					on pack.packid = res.lightpack
					INNER JOIN tblpackage_equip as peq
					ON peq.packid = pack.packid
					WHERE res.reservationid=".$u_id);

				while($r = $lighteq->fetch()){
					$stmt = $conn->prepare("UPDATE `tblequipments` SET `count`=`count` - :ct WHERE `eqid`=:eqid");
					$stmt->bindParam(':ct', $r['quantity'], PDO::PARAM_INT);
					$stmt->bindParam(':eqid',$r['eqid']);
					$stmt->execute(); 
				}


			//get all addons equipement
			//decrement that to database

				$addons = $conn->query(
					"SELECT ae.eqid, ae.quantity FROM `tblreservation_addons` as resa 
					INNER JOIN tbladdons as ado 
					ON ado.addonid = resa.addonid
					INNER JOIN tbladdons_equip as ae 
					on ae.addonid = ado.addonid
					WHERE resa.`reservationid`= ".$u_id);

				while($r = $addons->fetch()){
					$stmt = $conn->prepare("UPDATE `tblequipments` SET `count`=`count` - :ct WHERE `eqid`=:eqid");
					$stmt->bindParam(':ct', $r['quantity'], PDO::PARAM_INT);
					$stmt->bindParam(':eqid',$r['eqid']);
					$stmt->execute(); 
				}
				include '../config/mconfig.php';
			//update tblreseration , set status = $status
				$stmt = $conn->prepare("UPDATE `tblreservation` SET `status`=? WHERE reservationid=?");
				$stmt->bind_param("si", $status , $u_id);
				$stmt->execute();

			}else if($status == "Ongoing" || $status == "Pending" ){
				include '../config/mconfig.php';
				$stmt = $conn->prepare("UPDATE `tblreservation` SET `status`=? WHERE reservationid=?");
				$stmt->bind_param("si", $status , $u_id);
				$stmt->execute();
			}
		}
	}
	showAdmins();
}

?>