<?php 


if(session_id()){}else{session_start();}

if(isset($_POST['action']) && !empty($_POST['action']))
{

	$action = $_POST['action'];
	switch ($action) {
		case 'addEvents':
		addEvents();
		break;
		case 'showEvents':
		showEvents();
		break;
		case 'deleteEvent':
		deleteEvent();
		break;
		case 'showUpdateEvent':
		showUpdateEvent();
		break;
		case 'updateEvent':
		updateEvent();
		break;
		default:
				# code...
		break;
	}
}


function secure($str){
	return strip_tags(trim(htmlspecialchars($str)));
}


function ContainsNumbers($String){
	return preg_match('/\\d/', $String) > 0;
}

function addEvents()
{
	include '../config/mconfig.php';

	/*
		Validations 
	*/
		$errors = array();

		/*

		if(strlen($_POST['fname']) == 0){
			array_push($errors, "First Name can not be blank!");
		}else{
			if(ContainsNumbers($_POST['fname'])){
				array_push($errors, "First Name contains number!");
			}
		}
		*/

		
		if(count($errors) > 0 )
		{

			echo "<tr><script type='text/javascript'>
			$(document).ready(function(){
				$('#msgtitle').text('Error');
				$('#modalmsg').html(\"".implode("<br />",$errors)."\");
				$('#msgmodalbtn').text('Close');
				$('#msgmodalbtn').attr('class', 'btn btn-danger pull-right');
				$('#msgmodalheader').attr('class', 'modal-header modal-header-danger');
				$('#msgmodal').modal('show');



			});
		</script></tr>";
	}
	else
	{


		$eventname = secure($_POST['eventname']);
		$eventdesc = secure($_POST['eventdesc']);

		// prepare and bind
		$stmt = $conn->prepare("INSERT INTO `tblevents`( `eventname`, `eventdesc`) VALUES (?, ?)");
		$stmt->bind_param("ss", $eventname, $eventdesc);

		$stmt->execute();



		echo "<tr><script type='text/javascript'>
		$(document).ready(function(){
			$('#msgtitle').text('Success');
			$('#modalmsg').html('Event successfully added!');
			$('#msgmodalbtn').attr('class', 'btn btn-success pull-right');
			$('#msgmodalheader').attr('class', 'modal-header modal-header-success');
			$('#msgmodal').modal('show');
			$('#msgmodalbtn').text('Close');
		});
	</script></tr>";
}
showEvents();


}




function showEvents()
{
	include '../config/config.php';

	$admins = $conn->query("SELECT `eventid`, `eventname`, `eventdesc` FROM `tblevents`");

	while($r = $admins->fetch()){
		echo "<tr>";
		echo "<td>".$r['eventid']."</td>";
		echo "<td>".$r['eventname']."</td>";
		echo "<td>".$r['eventdesc']."</td>";

		echo '<td><a class="btn btn-sm btn-primary" href="gallery.php?eventid='.$r['eventid'].'"> <span class="glyphicon glyphicon-pencil"></span> Show</a> |<a class="btn btn-sm btn-info" onclick="updateEvent('.$r['eventid'].')"> <span class="glyphicon glyphicon-pencil"></span> Edit</a> | <a class="btn btn-sm btn-danger" onclick="deleteEvent('.$r['eventid'].')"><span class=
		"glyphicon glyphicon-trash"></span> Delete</a></td>';
		echo "</tr>";
	}
}

function deleteEvent()
{
	include '../config/config.php';
	$id = $_POST['id'];
    // prepare sql and bind parameters
	$stmt = $conn->prepare("DELETE FROM `tblevents` WHERE `eventid`=:id");
	$stmt->bindParam(':id', $id);
	$stmt->execute();


	echo "<tr><script type='text/javascript'>
	$(document).ready(function(){
		$('#msgtitle').text('Success');
		$('#modalmsg').html('Event successfully deleted!');
		$('#msgmodalbtn').text('Close');
		$('#msgmodalbtn').attr('class', 'btn btn-success pull-right');
		$('#msgmodalheader').attr('class', 'modal-header modal-header-success');
		$('#msgmodal').modal('show');
	});
	</script></tr>";


showEvents();
}

function showUpdateEvent()
{
	include '../config/config.php';
	$id = $_POST['id'];

	$stmt = $conn->prepare("SELECT `eventid`, `eventname`, `eventdesc` FROM `tblevents` WHERE `eventid`=:id");
	$stmt->bindParam(':id',$id);
	$stmt->execute(); 
	$row = $stmt->fetch();

	$eventid = secure($row['eventid']);
	$eventname = secure($row['eventname']);
	$eventdesc = secure($row['eventdesc']);


	echo json_encode(array(
		"eventid" => $eventid, 
		"eventname" => $eventname, 
		"eventdesc" => $eventdesc
	));

}	


function updateEvent()
{
	include '../config/mconfig.php';


	$errors = array();

	$u_id = secure($_POST['u_id']);

	$eventname = secure($_POST['eventname']);
	$eventdesc = secure($_POST['eventdesc']);


	/*

	ALERT ALERT ALERT ==== NO VALIDATION
	if(strlen($_POST['u_fname']) == 0){
		array_push($errors, "First Name can not be blank!");
	}else{
		if(ContainsNumbers($_POST['u_fname'])){
			array_push($errors, "First Name contains number!");
		}
	}


	*/



	if(count($errors) > 0 )
	{

		echo "<tr><script type='text/javascript'>
		$(document).ready(function(){
			$('#msgtitle').text('Error');
			$('#modalmsg').html(\"".implode("<br />",$errors)."\");
			$('#msgmodalbtn').text('Close');
			$('#msgmodalbtn').attr('class', 'btn btn-danger pull-right');
			$('#msgmodalheader').attr('class', 'modal-header modal-header-danger');
			$('#msgmodal').modal('show');
		});</script></tr>";
	}
	else
	{

	// prepare and bind
	$stmt = $conn->prepare("UPDATE `tblevents` SET `eventname`=?,`eventdesc`=? WHERE eventid=?");
	$stmt->bind_param("ssi", $eventname, $eventdesc, $u_id);

	$stmt->execute();


	echo "<tr><script type='text/javascript'>
	$(document).ready(function(){
		$('#msgtitle').text('Success');
		$('#modalmsg').html('Event successfully updated!');
		$('#msgmodalbtn').text('Close');
		$('#msgmodalbtn').attr('class', 'btn btn-success pull-right');
		$('#msgmodalheader').attr('class', 'modal-header modal-header-success');
		$('#msgmodal').modal('show');
	});
	</script></tr>";

	}




	showEvents();


}

?>