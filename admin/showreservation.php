<?php include 'auth.php'; ?> 
<?php include 'accountinfo.php'; ?>

<?php 
function secure($str){
  return strip_tags(trim(htmlspecialchars($str)));
}

if(isset($_GET['id'])){
  $id = $_GET['id'];
  $stmt = $conn->prepare("SELECT * FROM `tblreservation` as res
    INNER JOIN tblcustomer as cs 
    on cs.custid = res.custid
    where res.reservationid=:id");

  $stmt->bindParam(':id',$id);
  $stmt->execute(); 
  $resinfo = $stmt->fetch();

}else{
  header("Location: index.php");
}



?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title> DOMELJEN Events Unlimited Inc</title>

  <!-- Bootstrap -->
  <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- NProgress -->
  <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
  <!-- iCheck -->
  <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">

  <!-- bootstrap-progressbar -->
  <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
  <!-- JQVMap -->
  <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
  <!-- bootstrap-daterangepicker -->
  <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

  <!-- My Modal Stylesheet -->
  <link rel="stylesheet" type="text/css" href="../build/css/modalstyle.css">

  <!-- Custom Theme Style -->
  <link href="../build/css/custom.min.css" rel="stylesheet">

  <style type="text/css">
  body { padding-right: 0 !important }
</style>


</head>

<body class="nav-md" >
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <?php include('templates/admin.topnavtitle.php'); ?> 

          <div class="clearfix"></div>
          <?php include('templates/admin.quickinfo.php'); ?> 
          <br />

          <?php include('templates/admin.sidebar.php'); ?> 
          <?php include('templates/admin.menufooter.php'); ?> 

        </div>
      </div>

      <?php include('templates/admin.topnav.php'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="container">

          <div class="x_panel">
            <div class="x_title">
              <h2>Reservation Information </h2>
              <div class="clearfix"></div>

            </div>
            <div class="x_content">

              <form id="reserve-form">
               <div class="container">

                 <div class="col-md-7">

                   <h2>Customer Information</h2>
                   <div class="col-lg-12 well">
                    <div class="row">

                      <div class="col-sm-12">
                       <div class="row">
                        <div class="col-sm-6 form-group">
                         <label>First Name</label>
                         <input type="text" placeholder="Enter First Name Here.." class="form-control" name="firstname" pattern="[a-zA-Z\s]{1,}" title="Letters only!" required="" readonly="" value="<?php echo $resinfo['first_name']; ?>">
                       </div>
                       <div class="col-sm-6 form-group">
                         <label>Last Name</label>
                         <input type="text" placeholder="Enter Last Name Here.." class="form-control" name="lastname" pattern="[a-zA-Z\s]{1,}" title="Letters only!" required="" readonly="" value="<?php echo $resinfo['last_name']; ?>">
                       </div>
                     </div>
                     <div class="form-group">
                      <label>Address</label>
                      <textarea placeholder="Enter Address Here.." rows="3" class="form-control" name="address" readonly="" value="<?php echo $resinfo['address']; ?>"></textarea>
                    </div>
                    <div class="row">
                      <div class="col-sm-4 form-group">
                       <label>City</label>
                       <input type="text" placeholder="Enter City Name Here.." class="form-control" name="city" pattern="[a-zA-Z\s]{1,}" title="Letters only!" required="" readonly="" value="<?php echo $resinfo['city']; ?>">
                     </div>
                     <div class="col-sm-4 form-group">
                       <label>State</label>
                       <input type="text" placeholder="Enter State Name Here.." class="form-control" name="state" pattern="[a-zA-Z\s]{1,}" title="Letters only!" required="" readonly="" value="<?php echo $resinfo['state']; ?>">
                     </div>
                     <div class="col-sm-4 form-group">
                       <label>Zip</label>
                       <input type="text" placeholder="Enter Zip Code Here.." class="form-control" name="zip"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' readonly="" value="<?php echo $resinfo['zip']; ?>">
                     </div>
                   </div>
                   <div class="row">
                    <div class="col-sm-6 form-group">
                     <label>Title</label>
                     <input type="text" placeholder="Enter Designation Here.." class="form-control" name="title" readonly="" value="<?php echo $resinfo['title']; ?>">
                   </div>
                   <div class="col-sm-6 form-group">
                     <label>Company</label>
                     <input type="text" placeholder="Enter Company Name Here.." class="form-control" name="company" readonly="" value="<?php echo $resinfo['company']; ?>">
                   </div>
                 </div>
                 <div class="form-group">
                  <label>Phone Number</label>
                  <input type="text" placeholder="Enter Phone Number Here.." class="form-control" name="phone" required="" readonly="" value="<?php echo $resinfo['phone']; ?>">
                </div>
                <div class="form-group">
                  <label>Email Address</label>
                  <input type="email" placeholder="Enter Email Address Here.." class="form-control" name="email" required="" readonly="" value="<?php echo $resinfo['email']; ?>">
                  <input type="hidden" name="action" value="reserveNow">
                </div>
              </div>
            </div>
          </div>
          <h2>Add Ons</h2>
          <div class="col-lg-12 well">
            <div class="row">
             <div class="form-group">
              <label>Other Services</label>
              <div class="form-group ScrollStyle">

                <?php 
                include 'config/config.php';
                $admins = $conn->query("SELECT * FROM `tblreservation_addons` as ra 
                  INNER JOIN tbladdons as ad 
                  on ad.addonid = ra.`addonid`
                  WHERE reservationid=".$id);
                while($r = $admins->fetch()){
                  echo "<p class='pull-right'>PHP ".$r['price']."</p>
                  <div class='checkbox'>
                  <label>
                  <input class='addon' onchange='computeTotal()' type='checkbox' name='addons[]'  checked=''onclick='return false;' value='".$r['addonid']."'>".$r['addonname']."
                  </label>
                  </div>";
                  echo "<ul>";

                  $includes = $conn->query("SELECT `addeqid`, eq.name, `quantity` FROM `tbladdons_equip` as ae
                    INNER JOIN tblequipments as eq 
                    on eq.eqid = ae.`eqid`
                    WHERE `addonid`=".$r['addonid']);

                  while($rs = $includes->fetch()){
                    echo "<li>".$rs['quantity']." ".$rs['name']."</li>";
                  }

                  echo "</ul>";
                }
                ?>
              </div>
            </div>
          </div>
        </div>

      </div>
      <div class="col-md-5">
       <h2>Event, Package and Others</h2>
       <div class="col-lg-12 well">
        <div class="row">

          <div class="col-sm-12">
           <div class="form-group">
            <label>Event Date</label>
            <input id="date" type="date" name="eventdate" class="form-control" required="" readonly="">
            <label>Event Type</label>
            <select class="form-control" name="eventtype">
             <option value="">Select Event Type</option>
             <option value="Concerts" <?php if($resinfo['eventtype'] == "Concerts"){echo "selected"; } ?>>Concerts</option>
             <option value="Company Events" <?php if($resinfo['eventtype'] == "Company Events"){echo "selected"; } ?>>Company Events</option>
             <option value="School Events" <?php if($resinfo['eventtype'] == "School Events"){echo "selected"; } ?>>School Events</option>
             <option value="Religious Events" <?php if($resinfo['eventtype'] == "Religious Events"){echo "selected"; } ?>>Religious Events</option>
             <option value="Campus Events" <?php if($resinfo['eventtype'] == "Campus Events"){echo "selected"; } ?>>Campus Events</option>
             <option value="Fashion Shows" <?php if($resinfo['eventtype'] == "Fashion Shows"){echo "selected"; } ?>>Fashion Shows</option>
             <option value="Weddings" <?php if($resinfo['eventtype'] == "Weddings"){echo "selected"; } ?>>Weddings</option>
             <option value="Birthday/Debut" <?php if($resinfo['eventtype'] == "Birthday/Debut"){echo "selected"; } ?>>Birthday/Debut</option>
           </select>
         </div>

         <div class="form-group">
          <label>PROFESSIONAL AUDIO EQUIPMENTS Package</label>

        <?php 
        $light = $conn->query(
          "SELECT peq.`quantity`, eq.name  FROM `tblpackage_equip` peq
          INNER JOIN tblequipments as eq 
          ON eq.eqid = peq.`eqid`
          WHERE packid=".$resinfo['soundpack']);

        while($r = $light->fetch()){
          echo "<p>".$r['quantity']." ".$r['name']."</p>";
        }

        $stmt = $conn->prepare("SELECT * FROM `tblpackages` WHERE `packid`=:id");
        $stmt->bindParam(':id',$packid);
        $stmt->execute(); 
        $row = $stmt->fetch();



        ?>
      <div id="packinfo_sounds" class="ScrollStyle2">
      </div>
      <br/>
      <div class="form-group">
        <label>PROFESSIONAL LIGHTS SYSTEM Package</label>


        <?php 
        $light = $conn->query(
          "SELECT peq.`quantity`, eq.name  FROM `tblpackage_equip` peq
          INNER JOIN tblequipments as eq 
          ON eq.eqid = peq.`eqid`
          WHERE packid=".$resinfo['lightpack']);

        while($r = $light->fetch()){
          echo "<p>".$r['quantity']." ".$r['name']."</p>";
        }

        $stmt = $conn->prepare("SELECT * FROM `tblpackages` WHERE `packid`=:id");
        $stmt->bindParam(':id',$packid);
        $stmt->execute(); 
        $row = $stmt->fetch();



        ?>
      </div>


      <br/>
      <div class="form-group">
        <label>Coupon Code</label>
        <input type="text" name="coupon" id="promocode" class="form-control" value="<?php echo $resinfo['couponcode']; ?>" readonly>
      </div>
      <h2 >Grand Total</h2>
      <h1 class="pull-right" style="color: black;" id="gtotal">PHP <?php echo $resinfo['total']; ?></h1>
               
    </div>

  </div>

</div>
</div>

</div>
</div>
</form>





</div>
</div>  <!-- End of container -->
</div>
</div>
<!-- /page content -->
<?php include('templates/admin.footer.php'); ?>
</div>
</div>






<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="../vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="../vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="../vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="../vendors/Flot/jquery.flot.js"></script>
<script src="../vendors/Flot/jquery.flot.pie.js"></script>
<script src="../vendors/Flot/jquery.flot.time.js"></script>
<script src="../vendors/Flot/jquery.flot.stack.js"></script>
<script src="../vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="../vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="../vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="../build/js/custom.min.js"></script>


<div class="modal fade" id="msgmodal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div id="msgmodalheader" class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="msgtitle"></h4>
      </div>
      <div class="modal-body">
        <p id="modalmsg"></p>
      </div>
      <div class="modal-footer">
        <button type="button" id="msgmodalbtn" class="btn btn-danger pull-right" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



</body>
</html>
