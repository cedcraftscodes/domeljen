<?php 


if(session_id()){}else{session_start();}

if(isset($_POST['action']) && !empty($_POST['action']))
{

	$action = $_POST['action'];
	switch ($action) {
		case 'addPackage':
		addPackage();
		break;

		case 'showPackages':
		showPackages();
		break;

		case 'deletePack':
		deletePack();
		break;

		case 'updatePackage':
			updatePackage();
			break;
		default:
				# code...
		break;
	}
}





function deletePack()
{
	include '../config/config.php';
	$id = $_POST['id'];
    // prepare sql and bind parameters
	$stmt = $conn->prepare("DELETE FROM `tblpackages` WHERE `packid`=:id");
	$stmt->bindParam(':id', $id);
	$stmt->execute();

	$stmt = $conn->prepare("DELETE FROM `tblpackage_equip` WHERE `packid`=:id");
	$stmt->bindParam(':id', $id);
	$stmt->execute();


	echo "<tr><script type='text/javascript'>
	$(document).ready(function(){
		$('#msgtitle').text('Success');
		$('#modalmsg').html('Package successfully deleted!');
		$('#msgmodalbtn').text('Close');
		$('#msgmodalbtn').attr('class', 'btn btn-success pull-right');
		$('#msgmodalheader').attr('class', 'modal-header modal-header-success');
		$('#msgmodal').modal('show');
	});
	</script></tr>";


	showPackages();
}


function showPackages()
{
	include '../config/config.php';

	$admins = $conn->query("SELECT `packid`, `packname`,`packagetype`, `noOfCrew`, `Price` FROM `tblpackages`");

	while($r = $admins->fetch()){
		echo "<tr>";
		echo "<td>".$r['packid']."</td>";
		echo "<td>".$r['packname']."</td>";
		echo "<td>".$r['packagetype']."</td>";
		echo "<td>".$r['noOfCrew']."</td>";
		echo "<td>".$r['Price']."</td>";
		echo '<td><a class="btn btn-sm btn-info" href="editpackage.php?id='.$r['packid'].'"> <span class="glyphicon glyphicon-pencil"></span> Edit</a> | <a class="btn btn-sm btn-danger" onclick="deleteAdmin('.$r['packid'].')"><span class=
		"glyphicon glyphicon-trash"></span> Delete</a></td>';
		echo "</tr>";
	}
}






function updatePackage(){
	//delete 




	$quantities = $_POST['quantities'];
	$packname =  secure($_POST['packname']);
	$crew =  secure($_POST['crew']);
	$price =  secure($_POST['price']);
	$packid =  secure($_POST['packid']);

	$type =  secure($_POST['packagetype']);
	

	$totalqty = 0;

	foreach ($quantities as $id => $qty) 
	{
		$totalqty+=(int)$qty;
	}

	if($totalqty > 0){


		include '../config/config.php';
		$stmt = $conn->prepare("DELETE FROM tblpackage_equip WHERE `packid`=:id");
		$stmt->bindParam(':id', $packid);
		$stmt->execute(); 

		$time = time();

		$stmt = $conn->prepare("UPDATE `tblpackages` SET `packname`=:name,`noOfCrew`=:crew,`Price`=:price, `packagetype`=:type WHERE `packid`=:id");
		$stmt->bindParam(':type',$type);
		$stmt->bindParam(':name',$packname);
		$stmt->bindParam(':crew',$crew);
		$stmt->bindParam(':price',$price);
		$stmt->bindParam(':id',$packid);
		$stmt->execute(); 

		foreach ($quantities as $id => $qty) {
			$pid = $id;
			$quantity = (int)$qty;

			if($quantity == 0)
				continue;

			$stmt = $conn->prepare("INSERT INTO `tblpackage_equip`(`eqid`, `packid`, `quantity`) VALUES (:eq, :pid, :qty)");
			$stmt->bindParam(':eq',$id);
			$stmt->bindParam(':pid',$packid);
			$stmt->bindParam(':qty',$quantity);
			$stmt->execute(); 
			
		}

		echo json_encode(array(
			"message" => 'Updating packages is successful!', 
			"success" => true
		));

	}else{
		echo json_encode(array(
			"message" => 'Package must have atleast one equipment!', 
			"success" => false
		));
	}

}





function addPackage(){
	$quantities = $_POST['quantities'];
	$packname =  secure($_POST['packname']);
	$crew =  secure($_POST['crew']);
	$price =  secure($_POST['price']);
	$type = secure($_POST['packagetype']);

	$totalqty = 0;

	foreach ($quantities as $id => $qty) 
	{
		$totalqty+=(int)$qty;
	}

	if($totalqty > 0){
		$time = time();

		include '../config/config.php';
		$stmt = $conn->prepare("INSERT INTO `tblpackages`(`packname`, `packagetype`,`noOfCrew`, `Price`) VALUES (:name, :type, :crew, :price)");

		$stmt->bindParam(':type',$type);
		$stmt->bindParam(':name',$packname);
		$stmt->bindParam('crew',$crew);
		$stmt->bindParam(':price',$price);
		$stmt->execute(); 
		$delid = $conn->lastInsertId();


		foreach ($quantities as $id => $qty) {
			$pid = $id;
			$quantity = (int)$qty;

			if($quantity == 0)
				continue;

			$stmt = $conn->prepare("INSERT INTO `tblpackage_equip`(`eqid`, `packid`, `quantity`) VALUES (:eq, :pid, :qty)");
			$stmt->bindParam(':eq',$id);
			$stmt->bindParam(':pid',$delid);
			$stmt->bindParam(':qty',$quantity);
			$stmt->execute(); 
			
		}

		echo json_encode(array(
			"message" => 'Adding packages is successful!', 
			"success" => true
		));

	}else{

		echo json_encode(array(
			"message" => 'Package must have atleast one equipment!', 
			"success" => false
		));
	}

}




function secure($str){
	return strip_tags(trim(htmlspecialchars($str)));
}


function ContainsNumbers($String){
	return preg_match('/\\d/', $String) > 0;
}


?>