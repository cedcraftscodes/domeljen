<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>Dashboard</h3>

    <ul class="nav side-menu">
      <li>
        <a href="index.php"><i class="fa fa-calendar"></i>Reservations</a>
      </li>
    </ul>

    <ul class="nav side-menu">
      <li>
        <a href="packages.php"><i class="fa fa-archive"></i> Packages </a>
      </li>
    </ul>

    
    <ul class="nav side-menu">
      <li>
        <a href="addons.php"><i class="fa fa-server"></i> Services Add ons </a>
      </li>
    </ul>

    <ul class="nav side-menu">
      <li>
        <a href="equipments.php"><i class="fa fa-bars"></i> Equipments Inventory</a>
      </li>
    </ul>
    
    <ul class="nav side-menu">
      <li>
        <a href="events.php"><i class="fa fa-calendar"></i> Events</a>
      </li>
    </ul>

    <ul class="nav side-menu">
      <li>
        <a href="promos.php"><i class="fa fa-money"></i> Promos </a>
      </li>
    </ul>


<!--     <ul class="nav side-menu">
      <li>
        <a href="qoutation.php"><i class="fa fa-truck"></i> Qoutation </a>
      </li>
    </ul> -->

    <ul class="nav side-menu">
      <li>
        <a href="adminaccounts.php"><i class="fa fa-users"></i> Account Management </a>
      </li>
    </ul>

    <ul class="nav side-menu">
      <li>
        <a href="loginhistory.php"><i class="fa fa-book"></i> Login History </a>
      </li>
    </ul>
<!--     
    <ul class="nav side-menu">
      <li>
        <a href="companyinfos.php"><i class="fa fa-info-circle"></i> Company Information </a>
      </li>
    </ul> -->

    <ul class="nav side-menu">
      <li>
        <a href="profile.php"><i class="fa fa-user"></i> Profile </a>
      </li>
    </ul>
  </div>
</div>
